package org.mixteco.mixtecnotes;

import android.app.Activity;
import android.content.Intent;

import org.mixteco.mixtecnotes.home.MainActivity;
import org.mixteco.mixtecnotes.utils.SharedPrefUtils;

public class AppEntryActivity extends Activity {

    @Override
    protected void onResume() {
        super.onResume();
        SharedPrefUtils.clearSharedPreferences();
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}