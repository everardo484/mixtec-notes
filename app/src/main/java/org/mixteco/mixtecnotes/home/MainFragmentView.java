package org.mixteco.mixtecnotes.home;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.view.View;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;

import org.mixteco.mixtecnotes.model.Note;
import org.mixteco.mixtecnotes.mvp.MvpView;

public interface MainFragmentView extends MvpView {

    String getCategory();

    void setupRecyclerview(FirestoreRecyclerAdapter adapter);

    void setupUI();

    void showLoadingProgressBar();

    void hideLoadingProgressBar();

    void showBottomNavigationView();

    void hideBottomNavigationView();

    void onLongClick(View view, Note note);

    void showMessage(@StringRes int strResId);

    void playAudio(@NonNull View v, @NonNull Note note);

}
