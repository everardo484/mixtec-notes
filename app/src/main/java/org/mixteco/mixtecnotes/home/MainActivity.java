package org.mixteco.mixtecnotes.home;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.widget.FrameLayout;

import org.mixteco.mixtecnotes.BaseNavActivity;
import org.mixteco.mixtecnotes.R;
import org.mixteco.mixtecnotes.about.AboutFragment;
import org.mixteco.mixtecnotes.adapter.Category;
import org.mixteco.mixtecnotes.di.ApplicationComponent;
import org.mixteco.mixtecnotes.di.subcomponent.mainactivity.MainActivityModule;
import org.mixteco.mixtecnotes.utils.ForceUpdateChecker;

import javax.inject.Inject;

import butterknife.BindView;

public class MainActivity extends BaseNavActivity<MainActivityView, MainActivityPresenter>
        implements MainActivityView, ForceUpdateChecker.OnUpdateNeededListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Inject
    MainActivityPresenter presenter;

    @BindView(R.id.fragment_container)
    FrameLayout fragmentContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ForceUpdateChecker.with(this).onUpdateNeeded(this).check();
    }

    @Override
    protected Fragment createFragment(@IdRes int drawerNavId) {
        switch (drawerNavId) {
            case R.id.drawer_nav_home:
                return MainFragment.newInstance(Category.ABC);
            case R.id.drawer_nav_numbers:
                return MainFragment.newInstance(Category.NUMBERS);
            case R.id.drawer_nav_colors:
                return MainFragment.newInstance(Category.COLORS);
            case R.id.drawer_nav_family:
                return MainFragment.newInstance(Category.FAMILY);
            case R.id.drawer_nav_about_app:
                return AboutFragment.newInstance(new Bundle());

        }
        throw new IllegalStateException("Invalid state");
    }

    @Override
    public void navigateToLoginFlow() {
        super.navigateToLoginTemplate();
    }

    @Override
    public void showMessage(int strResId) {
        Snackbar.make(fragmentContainer, getString(strResId), Snackbar.LENGTH_SHORT).setAction(getString(R.string
                .snackbar_action), null).show();
    }

    @Override
    public boolean isLoggedIn() {
        return super.isLoggedIn();
    }

    @Override
    public void showAlertDialog(@NonNull AlertDialog dialog) {
        dialog.show();
    }

    @NonNull
    @Override
    public MainActivityPresenter getPresenter() {
        return presenter;
    }


    @Override
    public void onUpdateNeeded(final String updateUrl) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("New version available")
                .setMessage("Please, update app to new version to continue reposting.")
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectStore(updateUrl);
                            }
                        }).setNegativeButton("No, thanks",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).create();
        dialog.show();
    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    @Override
    public void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new MainActivityModule(this)).injectTo(this);
    }
}