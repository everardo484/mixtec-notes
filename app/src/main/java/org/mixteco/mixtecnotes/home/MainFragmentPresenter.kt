package org.mixteco.mixtecnotes.home

import android.net.Uri
import android.util.Log
import android.view.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.mixteco.mixtecnotes.R
import org.mixteco.mixtecnotes.adapter.NotesAdapter
import org.mixteco.mixtecnotes.model.DataRepository
import org.mixteco.mixtecnotes.model.Note
import org.mixteco.mixtecnotes.mvp.AbstractMvpPresenter

class MainFragmentPresenter(private val dataRepository: DataRepository) :
        AbstractMvpPresenter<MainFragmentView>(),
        NotesAdapter.OnDataChangeListener,
        NotesAdapter.OnItemLongClickListener,
        NotesAdapter.OnPlayAudioListener {

    private val disposable = CompositeDisposable()

    fun getConfiguredAdapter(category: String): NotesAdapter {
        val adapter = NotesAdapter(dataRepository.getOptions(category))
        adapter.setOnDataChangeListener(this)
        adapter.setOnItemLongClickListener(this)
        adapter.setOnPlayAudioListener(this)
        return adapter
    }

    override fun OnDataChanged() {
        ifViewAttached { view -> view.showBottomNavigationView() }
        ifViewAttached { view -> view.hideLoadingProgressBar() }
    }

    override fun onItemPlayAudio(v: View, note: Note) {
        ifViewAttached { view -> view.playAudio(v, note) }
    }

    override fun onLongClick(v: View, note: Note) {
        ifViewAttached { view -> view.onLongClick(v, note) }
    }

    fun deleteNote(note: Note) {
        disposable.add(dataRepository.delete(note)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError { ifViewAttached { view -> view.showMessage(R.string.delete_note_failed) } }
                .doOnComplete { onNoteDeleteSuccess(note) }
                .subscribe())
    }

    private fun onNoteDeleteSuccess(note: Note) {
        deleteAudioFileIfExist(note)
        ifViewAttached { view -> view.showMessage(R.string.delete_note_success) }
    }

    private fun deleteAudioFileIfExist(note: Note) {
        if (note.hasAudio()) {
            disposable.add(dataRepository.deleteAudioFile(Uri.parse(note.audio))
                    .subscribeOn(Schedulers.io())
                    .doOnError { e -> Log.d("BACKEND", "Unable to delete Audio File ${e.message}") }
                    .doOnComplete { Log.d("BACKEND", "Audio File was successfully deleted!") }
                    .subscribe())
        }
    }

    override fun onDestroy() {
        disposable.clear()
        super.onDestroy()
    }
}