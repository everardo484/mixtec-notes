package org.mixteco.mixtecnotes.home

import org.mixteco.mixtecnotes.model.DataRepository
import org.mixteco.mixtecnotes.mvp.AbstractMvpPresenter

class MainActivityPresenter(private val dataRepository: DataRepository) : AbstractMvpPresenter<MainActivityView>() {}