package org.mixteco.mixtecnotes.home;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;

import org.mixteco.mixtecnotes.R;
import org.mixteco.mixtecnotes.adapter.Category;
import org.mixteco.mixtecnotes.adapter.NotesAdapter;
import org.mixteco.mixtecnotes.add.AddNoteActivity;
import org.mixteco.mixtecnotes.common.BaseFragment;
import org.mixteco.mixtecnotes.utils.Config;
import org.mixteco.mixtecnotes.di.ApplicationComponent;
import org.mixteco.mixtecnotes.di.subcomponent.mainfragment.MainFragmentModule;
import org.mixteco.mixtecnotes.flashcard.FlashcardActivity;
import org.mixteco.mixtecnotes.model.Note;
import org.mixteco.mixtecnotes.quiz.QuizActivity;
import org.parceler.Parcels;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import hugo.weaving.DebugLog;

import static org.mixteco.mixtecnotes.utils.Config.NOTE;

public class MainFragment extends BaseFragment<MainFragmentView, MainFragmentPresenter>
        implements MainFragmentView,
        BottomNavigationView.OnNavigationItemSelectedListener,
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnCompletionListener {

    private static final String TAG = MainFragment.class.getSimpleName();

    private MainActivityView hostActivityView;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.loadingProgress)
    ProgressBar loadingProgress;

    @BindView(R.id.navigation)
    BottomNavigationView mBottomNavigationView;

    @Inject
    MainFragmentPresenter presenter;

    @Inject
    MediaPlayer mPlayer;

    private NotesAdapter mAdapter = null;
    private Animation bounce;

    private String mCategory;
    private Unbinder unbind;
    private ActionMode mActionMode = null;

    public static MainFragment newInstance(Category type) {
        final MainFragment fragment = new MainFragment();
        final Bundle args = new Bundle();
        args.putString(Config.Fragment.CATEGORY, type.name());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        hostActivityView = (MainActivityView) context;
    }

    @DebugLog
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        final Bundle args = getArguments();
        if (args == null) {
            throw new IllegalArgumentException(getResources().getString(R.string.fragIllegalArg));
        }
        mCategory = args.getString(Config.Fragment.CATEGORY);
        mPlayer.setOnPreparedListener(this);
        mPlayer.setOnCompletionListener(this);
        bounce = AnimationUtils.loadAnimation(getContext(), R.anim.bounce);
    }

    @DebugLog
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_main, container, false);
        unbind = ButterKnife.bind(this, view);
        setupUI();
        return view;
    }

    @DebugLog
    @Override
    public void onStart() {
        super.onStart();
        mAdapter.startListening();
    }

    @Override
    public void setupUI() {
        mAdapter = presenter.getConfiguredAdapter(mCategory);
        setupRecyclerview(mAdapter);
        mBottomNavigationView.setOnNavigationItemSelectedListener(this);
        mBottomNavigationView.getMenu().getItem(0).setCheckable(false);
    }

    @DebugLog
    @Override
    public void onStop() {
        super.onStop();
        mAdapter.stopListening();
        if (mActionMode != null) {
            mActionMode.finish();
        }
    }

    @Override
    public void setupRecyclerview(@NonNull FirestoreRecyclerAdapter adapter) {
        LinearLayoutManager llManager = new LinearLayoutManager(getContext());
        llManager.setOrientation(LinearLayoutManager.VERTICAL);
        llManager.setReverseLayout(true);
        llManager.setStackFromEnd(true);

        recyclerView.setLayoutManager(llManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.addOnScrollListener(onVerticalScrollListener);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent;
        mBottomNavigationView.getMenu().findItem(item.getItemId()).setCheckable(true);
        switch (item.getItemId()) {
            case R.id.bottom_nav_add_new:
                handleOnAddButtonClicked();
                break;
            case R.id.bottom_nav_flashcards:
                intent = new Intent(getContext(), FlashcardActivity.class);
                intent.putExtra(Config.Fragment.CATEGORY, getCategory());
                startActivity(intent);
                break;
            case R.id.bottom_nav_take_quiz:
                handleQuizClicked();
                break;
        }

        return true;
    }


    private void handleQuizClicked() {
        if (mAdapter.getItemCount() < 5) {
            showMessage(R.string.not_enough_items);
        } else {
            Intent intent = new Intent(getContext(), QuizActivity.class);
            intent.putExtra(Config.Fragment.CATEGORY, getCategory());
            startActivity(intent);
        }
    }

    @Override
    public void playAudio(@NonNull View v, @NonNull Note note) {
        bounce.reset();
        v.startAnimation(bounce);
        mPlayer.reset();
        try {
            mPlayer.setDataSource(note.getAudio());
            mPlayer.prepareAsync();
        } catch (IOException e) {
            Log.e(TAG, "prepare() failed");
        }
    }

    @DebugLog
    public void handleOnAddButtonClicked() {
        if (hostActivityView.isLoggedIn()) {
            navigateToAddNoteActivity();
        } else {
            hostActivityView.navigateToLoginFlow();
        }
    }

    private void navigateToAddNoteActivity() {
        Intent intent = new Intent(getContext(), AddNoteActivity.class);
        intent.putExtra(Config.Fragment.CATEGORY, getCategory());
        startActivity(intent);
    }

    @Override
    public String getCategory() {
        return mCategory;
    }

    @Override
    public void showLoadingProgressBar() {
        loadingProgress.setVisibility(View.VISIBLE);
    }

    @DebugLog
    @Override
    public void hideLoadingProgressBar() {
        loadingProgress.setVisibility(View.GONE);
    }

    @DebugLog
    @Override
    public void showBottomNavigationView() {
        mBottomNavigationView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideBottomNavigationView() {
        mBottomNavigationView.setVisibility(View.GONE);
    }

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new MainFragmentModule(this)).injectTo(this);
    }

    @NonNull
    @Override
    public MainFragmentPresenter getPresenter() {
        return presenter;
    }

    @DebugLog
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbind.unbind();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        bounce.cancel();
        bounce.reset();
        mPlayer.stop();
        mPlayer.reset();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mPlayer.start();
    }

    @Override
    public void onLongClick(View view, Note note) {
        if (hostActivityView.isLoggedIn()) {
            ((AppCompatActivity) view.getContext()).startSupportActionMode(onActionCallBack(note));
            view.setSelected(true);
        }
    }

    public void onEdit(@NonNull Note note) {
        Intent intent = new Intent(getContext(), AddNoteActivity.class);
        intent.putExtra(Config.Fragment.CATEGORY, getCategory());
        intent.putExtra(NOTE, Parcels.wrap(note));
        startActivity(intent);
    }

    @Override
    public void showMessage(@StringRes int strResId) {
        hostActivityView.showMessage(strResId);
    }

    public void onDelete(@NonNull Note note) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.delete_note));
        builder.setPositiveButton(android.R.string.yes, (dialog, id) -> {
            presenter.deleteNote(note);
            dialog.dismiss();
        });
        builder.setNegativeButton(android.R.string.cancel, (dialog, id) -> dialog.dismiss());
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private final OnVerticalScrollListener onVerticalScrollListener = new OnVerticalScrollListener() {
        @Override
        public void onScrolledUp() {
            mBottomNavigationView.setVisibility(View.VISIBLE);
        }

        @Override
        public void onScrolledDown() {
            mBottomNavigationView.setVisibility(View.GONE);
        }
    };

    public ActionMode.Callback onActionCallBack(@NonNull Note note) {
        return new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.context_menu, menu);
                mActionMode = mode;
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_delete:
                        onDelete(note);
                        break;
/*                    case R.id.action_edit:
                        onEdit(note);
                        break;*/
                }
                mode.finish();
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                mActionMode = null;
            }
        };
    }
}