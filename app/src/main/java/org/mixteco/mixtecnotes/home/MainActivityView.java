package org.mixteco.mixtecnotes.home;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;

import org.mixteco.mixtecnotes.mvp.MvpView;

public interface MainActivityView extends MvpView {

    void showMessage(@StringRes int strResId);

    void showAlertDialog(@NonNull AlertDialog dialog);

    boolean isLoggedIn();

    void navigateToLoginFlow();
}
