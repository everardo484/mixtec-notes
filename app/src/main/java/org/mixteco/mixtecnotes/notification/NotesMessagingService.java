package org.mixteco.mixtecnotes.notification;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import hugo.weaving.DebugLog;

public class NotesMessagingService extends FirebaseMessagingService {
    private static final String TAG = FirebaseMessagingService.class.getSimpleName();

    @DebugLog
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "Message From = " + remoteMessage.getFrom());
        Log.d(TAG, "Message Body = " + remoteMessage.getNotification().getBody());
    }

    @Override
    public void onNewToken(String newToken) {
        Log.d(TAG, "newToken: " + newToken);
    }
}
