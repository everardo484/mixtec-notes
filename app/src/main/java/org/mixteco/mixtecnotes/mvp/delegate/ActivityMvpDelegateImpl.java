package org.mixteco.mixtecnotes.mvp.delegate;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import org.mixteco.mixtecnotes.mvp.MvpPresenter;
import org.mixteco.mixtecnotes.mvp.MvpView;

public class ActivityMvpDelegateImpl<V extends MvpView, P extends MvpPresenter<V>> implements ActivityMvpDelegate {
    private static final String TAG = ActivityMvpDelegateImpl.class.getSimpleName();
    private final MvpDelegateCallback<V, P> mDelegateCallback;
    private final Activity mActivity;

    public ActivityMvpDelegateImpl(@NonNull final Activity activity,
                                   @NonNull final MvpDelegateCallback<V, P> delegateCallback) {
        mDelegateCallback = delegateCallback;
        mActivity = activity;
    }

    @Override
    public void onCreate(Bundle savedState) {
        getPresenter().attachView(getMvpView());
    }

    private P getPresenter() {
        return mDelegateCallback.getPresenter();
    }

    private V getMvpView() {
        V view = mDelegateCallback.getMvpView();
        if (view == null) {
            throw new NullPointerException("View returned from getMvpView() is null");
        }
        return view;
    }

    @Override
    public void onDestroy() {
        getPresenter().detachView();
        onDestroy(mActivity.isFinishing());
    }

    private void onDestroy(boolean isActivityFinishing) {
        if (isActivityFinishing) {
            Log.d(TAG, mActivity.getClass().getSimpleName()  + " is being killed");
            getPresenter().onDestroy();
        }
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onRestart() {

    }

    @Override
    public void onContentChanged() {

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
    }
}