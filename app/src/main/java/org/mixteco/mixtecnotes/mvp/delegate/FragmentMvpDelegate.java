package org.mixteco.mixtecnotes.mvp.delegate;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import org.mixteco.mixtecnotes.mvp.MvpPresenter;
import org.mixteco.mixtecnotes.mvp.MvpView;

public interface FragmentMvpDelegate<V extends MvpView, P extends MvpPresenter<V>> {

    void onAttach(Activity activity);

    void onAttach(Context context);

    void onCreate(Bundle saved);

    void onViewCreated(View view, @Nullable Bundle savedInstanceState);

    void onStart();

    void onResume();

    void onPause();

    void onStop();

    void onDestroyView();

    void onDestroy();

    void onDetach();

    void onActivityCreated(Bundle savedInstanceState);

    void onSaveInstanceState(Bundle outState);
}
