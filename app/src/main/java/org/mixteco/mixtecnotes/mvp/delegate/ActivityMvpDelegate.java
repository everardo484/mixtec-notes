package org.mixteco.mixtecnotes.mvp.delegate;

import android.os.Bundle;

import org.mixteco.mixtecnotes.mvp.MvpPresenter;
import org.mixteco.mixtecnotes.mvp.MvpView;

public interface ActivityMvpDelegate<V extends MvpView, P extends MvpPresenter<V>> {

    void onCreate(Bundle savedState);

    void onDestroy();

    void onPause();

    void onResume();

    void onStart();

    void onStop();

    void onRestart();

    void onContentChanged();

    void onSaveInstanceState(Bundle outState);

    void onPostCreate(Bundle savedInstanceState);
}
