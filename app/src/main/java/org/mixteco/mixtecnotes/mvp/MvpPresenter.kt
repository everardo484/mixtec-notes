package org.mixteco.mixtecnotes.mvp

import android.support.annotation.UiThread

interface MvpPresenter<V : MvpView> {

    @UiThread
    fun attachView(view: V)

    @UiThread
    fun detachView()

    @UiThread
    fun onDestroy()
}