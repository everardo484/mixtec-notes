package org.mixteco.mixtecnotes.mvp.delegate;

import android.support.annotation.NonNull;

import org.mixteco.mixtecnotes.mvp.MvpPresenter;
import org.mixteco.mixtecnotes.mvp.MvpView;

public interface MvpDelegateCallback<V extends MvpView, P extends MvpPresenter<V>> {

    @NonNull
    P getPresenter();

    V getMvpView();
}

