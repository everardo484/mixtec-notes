package org.mixteco.mixtecnotes;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;

import org.mixteco.mixtecnotes.common.BaseActivity;
import org.mixteco.mixtecnotes.login.SignOutListener;
import org.mixteco.mixtecnotes.mvp.MvpPresenter;
import org.mixteco.mixtecnotes.mvp.MvpView;

import butterknife.BindView;
import butterknife.ButterKnife;

import static org.mixteco.mixtecnotes.login.AuthenticationFacade.RC_SIGN_IN;

public abstract class BaseNavActivity<V extends MvpView, P extends MvpPresenter<V>> extends BaseActivity<V, P>
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.nav_view)
    NavigationView navView;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.fragment_container)
    FrameLayout fragmentContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer_nav_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        navView.setNavigationItemSelectedListener(this);

        if (savedInstanceState == null) {
            navView.getMenu().performIdentifierAction(R.id.drawer_nav_home, 0);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateLoginTextView();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        navView.setCheckedItem(item.getItemId());
        loadFragment(item.getItemId());
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void loadFragment(@IdRes int drawerNavId) {
        if (drawerNavId == R.id.drawer_nav_login) {
            handleLoginAction();
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, createFragment(drawerNavId))
                    .commit();
        }
    }

    public void handleLoginAction() {
        if (isLoggedIn()) {
            signOut(new SignOutListener() {
                @Override
                public void onLoggedOutSuccessfully() {
                    setTitleLogin();
                }

                @Override
                public void onLoggedOutFailed() {
                    setTitleLogout();
                }
            });
        } else {
            navigateToLoginTemplate();
        }
    }

    private void updateLoginTextView() {
        if (isLoggedIn()) {
            setTitleLogout();
        } else {
            setTitleLogin();
        }
    }

    protected void setTitleLogout() {
        navView.getMenu().findItem(R.id.drawer_nav_login).setTitle("Log Out");
    }

    protected void setTitleLogin() {
        navView.getMenu().findItem(R.id.drawer_nav_login).setTitle("Log In");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                setTitleLogout();
            } else {
                setTitleLogin();
            }
        }
    }

    protected abstract Fragment createFragment(@IdRes int drawerNavId);
}