package org.mixteco.mixtecnotes.model;

import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

import static org.mixteco.mixtecnotes.utils.Config.EMPTY;

@Parcel
public class Note {
    @SerializedName("author")
    @Expose
    public String author;
    @SerializedName("audio")
    @Expose
    public String audio;
    @SerializedName("category")
    @Expose
    public String category;
    @SerializedName("englishWord")
    @Expose
    public String englishWord;
    @SerializedName("key")
    @Expose
    public String key;
    @SerializedName("mixtecTranslation")
    @Expose
    public String mixtecTranslation;
    @SerializedName("pos")
    @Expose
    public String pos;
    @SerializedName("region")
    @Expose
    public String region;

    @SerializedName("timestamp")
    @Expose
    public Date timestamp;

    public String getAuthor() {
        return author;
    }

    @ServerTimestamp
    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getEnglishWord() {
        return englishWord;
    }

    public void setEnglishWord(String englishWord) {
        this.englishWord = englishWord;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMixtecTranslation() {
        return mixtecTranslation;
    }

    public void setMixtecTranslation(String mixtecTranslation) {
        this.mixtecTranslation = mixtecTranslation;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public boolean hasAuthor() {
        return author != null && !author.equals(EMPTY);
    }

    public boolean hasAudio() {
        return audio != null && !audio.equals(EMPTY);
    }

    public boolean hasPartOfSpeech() {
        return pos != null && !pos.equals(EMPTY);
    }
}