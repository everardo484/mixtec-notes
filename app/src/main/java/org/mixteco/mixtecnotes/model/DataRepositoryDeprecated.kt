/*
package org.mixteco.mixtecnotes.model

import android.content.ContentValues
import android.net.Uri
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import io.reactivex.Completable
import io.reactivex.Single
import org.mixteco.mixtecnotes.adapter.Category
import org.mixteco.mixtecnotes.utils.Config
import java.util.*
import javax.inject.Singleton

@Singleton
object DataRepositoryDeprecated {
    private val mFirebaseDatabase: FirebaseDatabase = FirebaseDatabase.getInstance()
    private val mFirebaseStorage: FirebaseStorage = FirebaseStorage.getInstance()
    private val NOTES = "notes"

    fun set(values: ContentValues): Completable {
        val note = getNote(values)
        val ref = getRootDatabaseReference().child(note.getCategory()).push()
        note.setKey(ref.key)
        return pushNote(note, ref)
    }

    fun set(key: String, values: ContentValues): Completable {
        val note = getNote(values)
        note.setKey(key)
        return pushNote(note, getRootDatabaseReference().child(note.getCategory()).child(key))
    }

    fun putAudioFile(path: Uri, type: Category): Completable {
        val audioRef = getAudioReference(type).child(path.lastPathSegment)
        return Completable.create { emitter ->
            audioRef.putFile(path).addOnSuccessListener {
                if (!emitter.isDisposed) {
                    emitter.onComplete()
                }
            }.addOnFailureListener { e ->
                if (!emitter.isDisposed) {
                    emitter.onError(e)
                }
            }
        }
    }

    fun getDownloadUri(path: Uri, type: Category) : Single<Uri> {
        val audioRef = getAudioReference(type).child(path.lastPathSegment)
        return Single.create{ emitter ->
            audioRef.downloadUrl.addOnSuccessListener {
                if (!emitter.isDisposed) {
                    emitter.onSuccess(it)
                }
            }.addOnFailureListener {
                if (!emitter.isDisposed) {
                    emitter.onError(it)
                }
            }
        }

    }

    fun deleteNote(note: Note): Completable {
        val ref = getRootDatabaseReference().child(note.getCategory()).child(note.getKey())
        return Completable.create { emitter ->
            ref.removeValue().addOnSuccessListener {
                if (!emitter.isDisposed) {
                    emitter.onComplete()
                }
            }.addOnFailureListener { e ->
                if (!emitter.isDisposed) {
                    emitter.onError(e)
                }
            }
        }
    }

    fun deleteAudioFile(uri: Uri): Completable {
        val ref = getRootAudioStorageReference().child(uri.lastPathSegment)
        return Completable.create { emitter ->
            ref.onDelete().addOnSuccessListener {
                if (!emitter.isDisposed) {
                    emitter.onComplete()
                }
            }.addOnFailureListener { e ->
                if (!emitter.isDisposed) {
                    emitter.onError(e)
                }
            }
        }
    }

    private fun getRootDatabaseReference(): DatabaseReference {
        return mFirebaseDatabase.reference.child(NOTES)
    }

    private fun getRootAudioStorageReference(): StorageReference {
        return mFirebaseStorage.reference
    }

    fun getDatabaseReference(type: Category): DatabaseReference {
        return getRootDatabaseReference().child(type.name)
    }

    private fun getAudioReference(type: Category): StorageReference {
        return getRootAudioStorageReference().child(type.name)
    }

    fun getAllNotesFrom(type: Category): Single<ArrayList<Note>> {
        val ref = getDatabaseReference(type)
        return Single.create { emitter ->
            ref.addListenerForSingleValueEvent(object : ValueEventListener {

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    if (!emitter.isDisposed) {
                        emitter.onSuccess(getNotesFromSnapshot(dataSnapshot))
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    if (!emitter.isDisposed) {
                        emitter.onError(Throwable(error.message))
                    }
                    ref.removeEventListener(this)
                }
            })
        }
    }

    private fun getNotesFromSnapshot(dataSnapshot: DataSnapshot) : ArrayList<Note> {
        val temp = ArrayList<Note>()
        for (snapshot in dataSnapshot.children) {
            val note = snapshot.getValue(Note::class.java)
            if (note != null) {
                temp.add(note)
            }
        }
        return temp
    }

    private fun pushNote(note: Note, ref: DatabaseReference): Completable {
        return Completable.create { emitter ->
            ref.set(note).addOnSuccessListener {
                if (!emitter.isDisposed) {
                    emitter.onComplete()
                }
            }.addOnFailureListener { e ->
                if (!emitter.isDisposed) {
                    emitter.onError(e)
                }
            }
        }
    }

    private fun getNote(values: ContentValues): Note {
        val note = Note()
        note.setEnglishWord(values.getAsString(Config.NOTE_ENGLISH))
        note.setMixtecTranslation(values.getAsString(Config.NOTE_MIXTEC))
        note.setPos(values.getAsString(Config.PART_OF_SPEECH))
        note.setCategory(values.getAsString(Config.NOTE_CATEGORY))
        note.setRegion(values.getAsString(Config.NOTE_REGION))
        note.setAudio(values.getAsString(Config.NOTE_AUDIO))
        return note
    }
}*/
