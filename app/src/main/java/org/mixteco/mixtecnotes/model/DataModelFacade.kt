package org.mixteco.mixtecnotes.model

import android.net.Uri
import android.provider.MediaStore
import android.support.annotation.NonNull
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import hugo.weaving.DebugLog
import io.reactivex.Completable
import io.reactivex.Single
import org.mixteco.mixtecnotes.adapter.Category
import org.mixteco.mixtecnotes.utils.Config

class DataModelFacade(private val db: FirebaseFirestore, private val storage: FirebaseStorage) {
    private val notes: String = "notes"
    private val timestamp: String = "timestamp"

    fun getOptionsOrderByDate(@NonNull category: String, @NonNull queryDirection: Query.Direction):
            FirestoreRecyclerOptions<Note> {

        val query: Query = db.collection(notes)
                .whereEqualTo(Config.Fragment.CATEGORY, category)
                .orderBy(timestamp, queryDirection)

        return FirestoreRecyclerOptions.Builder<Note>()
                .setQuery(query, Note::class.java)
                .build()
    }


    fun post(@NonNull note: Note): Completable {
        note.key = db.collection(notes).document().id
        return update(note)
    }

    fun update(@NonNull note: Note): Completable {
        return Completable.create { emitter ->
            db.collection(notes).document(note.key).set(note)
                    .addOnSuccessListener {
                        if (!emitter.isDisposed) {
                            emitter.onComplete()
                        }
                    }.addOnFailureListener { e ->
                        if (!emitter.isDisposed) {
                            emitter.onError(e)
                        }
                    }
        }
    }

    fun delete(@NonNull note: Note): Completable {
        return Completable.create { emitter ->
            db.collection(notes).document(note.key).delete()
                    .addOnSuccessListener {
                        if (!emitter.isDisposed) {
                            emitter.onComplete()
                        }
                    }.addOnFailureListener { e ->
                        if (!emitter.isDisposed) {
                            emitter.onError(e)
                        }
                    }
        }
    }

    fun fetch(): Single<List<Note>> {
        return Single.create { emitter ->
            db.collection(notes)
                    .get()
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            val items = ArrayList<Note>()
                            for (document in task.result!!) {
                                items.add(document.toObject(Note::class.java))
                            }
                            emitter.onSuccess(items)
                        } else {
                            emitter.onError(task.exception!!)
                        }
                    }
        }
    }

    @DebugLog
    fun fetch(@NonNull category: String): Single<List<Note>> {
        return Single.create { emitter ->
            db.collection(notes)
                    .whereEqualTo(MediaStore.Video.VideoColumns.CATEGORY, category)
                    .get()
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            val items = ArrayList<Note>()
                            for (document in task.result!!) {
                                items.add(document.toObject(Note::class.java))
                            }
                            emitter.onSuccess(items)
                        } else {
                            emitter.onError(task.exception!!)
                        }
                    }
        }
    }

    fun putAudioFile(@NonNull path: Uri, @NonNull category: Category): Completable {
        val audioRef = getAudioReference(category).child(path.lastPathSegment)
        return Completable.create { emitter ->
            audioRef.putFile(path).addOnSuccessListener {
                if (!emitter.isDisposed) {
                    emitter.onComplete()
                }
            }.addOnFailureListener { e ->
                if (!emitter.isDisposed) {
                    emitter.onError(e)
                }
            }
        }
    }

    fun getDownloadUri(@NonNull path: Uri, @NonNull type: Category): Single<Uri> {
        val audioRef = getAudioReference(type).child(path.lastPathSegment)
        return Single.create { emitter ->
            audioRef.downloadUrl.addOnSuccessListener {
                if (!emitter.isDisposed) {
                    emitter.onSuccess(it)
                }
            }.addOnFailureListener {
                if (!emitter.isDisposed) {
                    emitter.onError(it)
                }
            }
        }
    }

    fun deleteAudioFile(@NonNull uri: Uri): Completable {
        val ref = getRootAudioStorageReference().child(uri.lastPathSegment)
        return Completable.create { emitter ->
            ref.delete().addOnSuccessListener {
                if (!emitter.isDisposed) {
                    emitter.onComplete()
                }
            }.addOnFailureListener { e ->
                if (!emitter.isDisposed) {
                    emitter.onError(e)
                }
            }
        }
    }

    private fun getRootAudioStorageReference(): StorageReference {
        return storage.reference
    }

    private fun getAudioReference(category: Category): StorageReference {
        return getRootAudioStorageReference().child(category.name)
    }
}