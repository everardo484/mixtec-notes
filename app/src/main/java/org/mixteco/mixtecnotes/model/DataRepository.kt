package org.mixteco.mixtecnotes.model

import android.net.Uri
import android.support.annotation.NonNull
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.Query
import io.reactivex.Completable
import io.reactivex.Single
import org.mixteco.mixtecnotes.adapter.Category

class DataRepository(private val facade: DataModelFacade) {

    fun post(@NonNull note: Note): Completable {
        if (note.getKey() == null || note.getKey().isEmpty()) {
            return facade.post(note)
        }
        return facade.update(note)
    }

    fun delete(@NonNull note: Note): Completable {
        return facade.delete(note)
    }

    fun fetch(): Single<List<Note>> {
        return facade.fetch()
    }

    fun fetch(@NonNull category: String): Single<List<Note>> {
        return facade.fetch(category)
    }

    fun getOptions(@NonNull category: String): FirestoreRecyclerOptions<Note> {
        return facade.getOptionsOrderByDate(category, Query.Direction.ASCENDING)
    }

    fun putAudioFile(@NonNull path: Uri, @NonNull category: Category): Completable {
        return facade.putAudioFile(path, category)
    }

    fun getDownloadUri(@NonNull path: Uri, @NonNull category: Category): Single<Uri> {
        return facade.getDownloadUri(path, category)
    }

    fun deleteAudioFile(@NonNull uri: Uri): Completable {
        return facade.deleteAudioFile(uri)
    }
}