package org.mixteco.mixtecnotes.about;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.mixteco.mixtecnotes.R;
import org.mixteco.mixtecnotes.common.BaseFragment;
import org.mixteco.mixtecnotes.di.ApplicationComponent;
import org.mixteco.mixtecnotes.di.subcomponent.aboutfragment.AboutFragmentModule;

import javax.inject.Inject;

public class AboutFragment extends BaseFragment<AboutFragmentView, AboutFragmentPresenter>
        implements AboutFragmentView {
    @Inject
    AboutFragmentPresenter presenter;

    public static AboutFragment newInstance(Bundle args) {
        AboutFragment aboutFragment = new AboutFragment();
        aboutFragment.setArguments(args);
        return aboutFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about, container, false);
    }

    @NonNull
    @Override
    public AboutFragmentPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new AboutFragmentModule(this)).injectTo(this);
    }
}