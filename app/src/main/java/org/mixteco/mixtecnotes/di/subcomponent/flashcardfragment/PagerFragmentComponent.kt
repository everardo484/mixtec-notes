package org.mixteco.mixtecnotes.di.subcomponent.flashcardfragment

import dagger.Subcomponent
import org.mixteco.mixtecnotes.di.scope.FragmentScope
import org.mixteco.mixtecnotes.flashcard.PagerFragment

@FragmentScope
@Subcomponent(modules = [PagerFragmentModule::class])
interface PagerFragmentComponent {

    fun injectTo(fragment: PagerFragment)
}