package org.mixteco.mixtecnotes.di.subcomponent.mainfragment

import dagger.Subcomponent
import org.mixteco.mixtecnotes.di.scope.FragmentScope
import org.mixteco.mixtecnotes.home.MainFragment

@FragmentScope
@Subcomponent(modules = [MainFragmentModule::class])
interface MainFragmentComponent {

    fun injectTo(fragment: MainFragment)
}