package org.mixteco.mixtecnotes.di.subcomponent.aboutfragment

import dagger.Subcomponent
import org.mixteco.mixtecnotes.about.AboutFragment
import org.mixteco.mixtecnotes.di.scope.FragmentScope

@FragmentScope
@Subcomponent(modules = [AboutFragmentModule::class])
interface AboutFragmentComponent {

    fun injectTo(fragment : AboutFragment)
}