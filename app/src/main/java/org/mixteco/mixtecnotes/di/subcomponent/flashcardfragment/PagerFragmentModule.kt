package org.mixteco.mixtecnotes.di.subcomponent.flashcardfragment

import android.media.MediaPlayer
import dagger.Module
import dagger.Provides
import org.mixteco.mixtecnotes.di.FragmentModule
import org.mixteco.mixtecnotes.di.scope.FragmentScope
import org.mixteco.mixtecnotes.flashcard.PagerFragment
import org.mixteco.mixtecnotes.flashcard.PagerFragmentPresenter
import org.mixteco.mixtecnotes.flashcard.PagerFragmentView

@Module
class PagerFragmentModule(fragment: PagerFragment) : FragmentModule(fragment) {

    @FragmentScope
    @Provides
    fun providePagerFragmentView() : PagerFragmentView = fragment as PagerFragmentView

    @FragmentScope
    @Provides
    fun providePagerFragmentPresenter() : PagerFragmentPresenter = PagerFragmentPresenter()

    @FragmentScope
    @Provides
    fun provideMediaPlayer() : MediaPlayer = MediaPlayer()
}