package org.mixteco.mixtecnotes.di.subcomponent.quizactivity

import dagger.Module
import dagger.Provides
import org.mixteco.mixtecnotes.di.ActivityModule
import org.mixteco.mixtecnotes.di.scope.ActivityScope
import org.mixteco.mixtecnotes.model.DataRepository
import org.mixteco.mixtecnotes.quiz.QuizActivity
import org.mixteco.mixtecnotes.quiz.QuizActivityPresenter

@Module
class QuizActivityModule(activity: QuizActivity) : ActivityModule(activity) {

    @ActivityScope
    @Provides
    fun providesQuizActivityPresenter(dataRepository: DataRepository) : QuizActivityPresenter
            = QuizActivityPresenter(dataRepository)
}