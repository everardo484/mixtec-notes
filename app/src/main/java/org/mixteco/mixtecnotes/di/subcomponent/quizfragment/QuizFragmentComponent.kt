package org.mixteco.mixtecnotes.di.subcomponent.quizfragment

import dagger.Subcomponent
import org.mixteco.mixtecnotes.di.scope.FragmentScope
import org.mixteco.mixtecnotes.quiz.QuizFragment

@FragmentScope
@Subcomponent(modules = [QuizFragmentModule::class])
interface QuizFragmentComponent {

    fun injectTo(fragment: QuizFragment)
}