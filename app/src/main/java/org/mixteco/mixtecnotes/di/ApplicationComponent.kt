package org.mixteco.mixtecnotes.di

import dagger.Component
import org.mixteco.mixtecnotes.di.subcomponent.aboutfragment.AboutFragmentComponent
import org.mixteco.mixtecnotes.di.subcomponent.aboutfragment.AboutFragmentModule
import org.mixteco.mixtecnotes.di.subcomponent.addnoteactivity.AddNoteActivityComponent
import org.mixteco.mixtecnotes.di.subcomponent.addnoteactivity.AddNoteActivityModule
import org.mixteco.mixtecnotes.di.subcomponent.addnotefragment.AddNoteFragmentComponent
import org.mixteco.mixtecnotes.di.subcomponent.addnotefragment.AddNoteFragmentModule
import org.mixteco.mixtecnotes.di.subcomponent.flashcardactivity.FlashcardActivityComponent
import org.mixteco.mixtecnotes.di.subcomponent.flashcardactivity.FlashcardActivityModule
import org.mixteco.mixtecnotes.di.subcomponent.flashcardfragment.PagerFragmentComponent
import org.mixteco.mixtecnotes.di.subcomponent.flashcardfragment.PagerFragmentModule
import org.mixteco.mixtecnotes.di.subcomponent.mainactivity.MainActivityComponent
import org.mixteco.mixtecnotes.di.subcomponent.mainactivity.MainActivityModule
import org.mixteco.mixtecnotes.di.subcomponent.mainfragment.MainFragmentComponent
import org.mixteco.mixtecnotes.di.subcomponent.mainfragment.MainFragmentModule
import org.mixteco.mixtecnotes.di.subcomponent.quizactivity.QuizActivityComponent
import org.mixteco.mixtecnotes.di.subcomponent.quizactivity.QuizActivityModule
import org.mixteco.mixtecnotes.di.subcomponent.quizfragment.QuizFragmentComponent
import org.mixteco.mixtecnotes.di.subcomponent.quizfragment.QuizFragmentModule
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun plus(module: MainActivityModule) : MainActivityComponent

    fun plus(module: AddNoteFragmentModule) : AddNoteFragmentComponent

    fun plus(module: AddNoteActivityModule) : AddNoteActivityComponent

    fun plus(module: FlashcardActivityModule) : FlashcardActivityComponent

    fun plus(module: AboutFragmentModule) : AboutFragmentComponent

    fun plus(module: QuizActivityModule) : QuizActivityComponent

    fun plus(module: PagerFragmentModule) : PagerFragmentComponent

    fun plus(module: QuizFragmentModule) : QuizFragmentComponent

    fun plus(module: MainFragmentModule) : MainFragmentComponent
}