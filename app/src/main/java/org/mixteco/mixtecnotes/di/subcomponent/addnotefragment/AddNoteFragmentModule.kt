package org.mixteco.mixtecnotes.di.subcomponent.addnotefragment

import android.media.MediaPlayer
import dagger.Module
import dagger.Provides
import org.mixteco.mixtecnotes.add.AddNoteFragment
import org.mixteco.mixtecnotes.add.AddNoteFragmentPresenter
import org.mixteco.mixtecnotes.add.AddNoteFragmentView
import org.mixteco.mixtecnotes.di.FragmentModule
import org.mixteco.mixtecnotes.di.scope.FragmentScope
import org.mixteco.mixtecnotes.model.DataRepository

@Module
class AddNoteFragmentModule(fragment: AddNoteFragment) : FragmentModule(fragment) {

    @Provides
    @FragmentScope
    fun providesAddNoteFragmentView() : AddNoteFragmentView = fragment as AddNoteFragmentView

    @Provides
    @FragmentScope
    fun providesAddNoteFragmentPresenter(repository : DataRepository) : AddNoteFragmentPresenter =
            AddNoteFragmentPresenter(repository)

    @Provides
    @FragmentScope
    fun provideMediaPlayer() : MediaPlayer = MediaPlayer()
}