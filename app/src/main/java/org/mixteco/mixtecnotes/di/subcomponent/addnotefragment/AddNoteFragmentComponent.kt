package org.mixteco.mixtecnotes.di.subcomponent.addnotefragment

import dagger.Subcomponent
import org.mixteco.mixtecnotes.add.AddNoteFragment
import org.mixteco.mixtecnotes.di.scope.FragmentScope

@FragmentScope
@Subcomponent(modules = [AddNoteFragmentModule::class])
interface AddNoteFragmentComponent {

  fun injectTo(fragment: AddNoteFragment)
}