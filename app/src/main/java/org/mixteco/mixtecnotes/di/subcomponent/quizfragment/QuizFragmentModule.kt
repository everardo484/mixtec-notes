package org.mixteco.mixtecnotes.di.subcomponent.quizfragment

import android.media.MediaPlayer
import dagger.Module
import dagger.Provides
import org.mixteco.mixtecnotes.di.FragmentModule
import org.mixteco.mixtecnotes.di.scope.FragmentScope
import org.mixteco.mixtecnotes.quiz.QuizFragment
import org.mixteco.mixtecnotes.quiz.QuizFragmentPresenter

@Module
class QuizFragmentModule(fragment: QuizFragment) : FragmentModule(fragment) {

    @Provides
    @FragmentScope
    fun providesQuizFragmentPresenter() : QuizFragmentPresenter = QuizFragmentPresenter()

    @Provides
    @FragmentScope
    fun provideMediaPlayer() : MediaPlayer = MediaPlayer()
}