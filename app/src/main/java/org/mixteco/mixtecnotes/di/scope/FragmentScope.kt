package org.mixteco.mixtecnotes.di.scope

import javax.inject.Scope

@Scope
annotation class FragmentScope