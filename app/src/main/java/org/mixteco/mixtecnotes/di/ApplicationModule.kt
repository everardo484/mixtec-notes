package org.mixteco.mixtecnotes.di

import android.content.Context
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.storage.FirebaseStorage
import dagger.Module
import dagger.Provides
import org.mixteco.mixtecnotes.MixtecNotesApp
import org.mixteco.mixtecnotes.di.qualifier.ApplicationQualifier
import org.mixteco.mixtecnotes.model.DataModelFacade
import org.mixteco.mixtecnotes.model.DataRepository
import java.util.*
import javax.inject.Singleton

@Module
class ApplicationModule(private val app: MixtecNotesApp) {

    @Provides
    @Singleton
    fun provideApplication(): MixtecNotesApp = app

    @Provides
    @Singleton
    @ApplicationQualifier
    fun provideApplicationContext(): Context = app


    @Provides
    @Singleton
    fun provideFirebaseFirestoreSettings(): FirebaseFirestoreSettings {
        return FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build()
    }

    @Provides
    @Singleton
    fun provideFirebaseFirestore(settings: FirebaseFirestoreSettings): FirebaseFirestore {
        val firebaseFirestore: FirebaseFirestore = FirebaseFirestore.getInstance();
        firebaseFirestore.firestoreSettings = settings
        return firebaseFirestore
    }

    @Provides
    @Singleton
    fun provideFirebaseStorage(): FirebaseStorage = FirebaseStorage.getInstance()

    @Provides
    @Singleton
    fun provideDataModelFacade(db: FirebaseFirestore, storage: FirebaseStorage): DataModelFacade = DataModelFacade(db, storage)

    @Provides
    @Singleton
    fun provideDataRepository(facade: DataModelFacade): DataRepository = DataRepository(facade)

    @Provides
    @Singleton
    fun provideRandom(): Random = Random()
}