package org.mixteco.mixtecnotes.di.subcomponent.addnoteactivity

import dagger.Module
import dagger.Provides
import org.mixteco.mixtecnotes.add.AddNoteActivity
import org.mixteco.mixtecnotes.add.AddNoteActivityPresenter
import org.mixteco.mixtecnotes.di.ActivityModule
import org.mixteco.mixtecnotes.di.scope.ActivityScope

@Module
class AddNoteActivityModule(activity: AddNoteActivity) : ActivityModule(activity) {

    @ActivityScope
    @Provides
    fun providesAddNoteActivityPresenter() : AddNoteActivityPresenter = AddNoteActivityPresenter()

}