package org.mixteco.mixtecnotes.di.subcomponent.mainfragment

import android.media.MediaPlayer
import dagger.Module
import dagger.Provides
import org.mixteco.mixtecnotes.di.FragmentModule
import org.mixteco.mixtecnotes.di.scope.FragmentScope
import org.mixteco.mixtecnotes.home.MainFragment
import org.mixteco.mixtecnotes.home.MainFragmentPresenter
import org.mixteco.mixtecnotes.model.DataRepository

@Module
class MainFragmentModule(fragment: MainFragment) : FragmentModule(fragment) {

    @Provides
    @FragmentScope
    fun provideMainFragmentPresenter(dataRepository: DataRepository):
            MainFragmentPresenter = MainFragmentPresenter(dataRepository)

    @Provides
    @FragmentScope
    fun provideMediaPlayer(): MediaPlayer = MediaPlayer()
}