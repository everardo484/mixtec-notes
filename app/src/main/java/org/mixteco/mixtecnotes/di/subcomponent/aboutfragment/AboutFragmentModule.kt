package org.mixteco.mixtecnotes.di.subcomponent.aboutfragment

import dagger.Module
import dagger.Provides
import org.mixteco.mixtecnotes.about.AboutFragment
import org.mixteco.mixtecnotes.about.AboutFragmentPresenter
import org.mixteco.mixtecnotes.di.FragmentModule
import org.mixteco.mixtecnotes.di.scope.FragmentScope

@Module
class AboutFragmentModule(fragment: AboutFragment) : FragmentModule(fragment) {

    @FragmentScope
    @Provides
    fun provideAboutFragmentPresenter(): AboutFragmentPresenter = AboutFragmentPresenter()

}