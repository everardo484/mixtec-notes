package org.mixteco.mixtecnotes.di.subcomponent.addnoteactivity

import dagger.Subcomponent
import org.mixteco.mixtecnotes.add.AddNoteActivity
import org.mixteco.mixtecnotes.di.scope.ActivityScope

@ActivityScope
@Subcomponent(modules = [AddNoteActivityModule::class])
interface AddNoteActivityComponent {

    fun injectTo(activity: AddNoteActivity)
}