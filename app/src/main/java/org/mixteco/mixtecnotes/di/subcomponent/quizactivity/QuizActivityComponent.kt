package org.mixteco.mixtecnotes.di.subcomponent.quizactivity

import dagger.Subcomponent
import org.mixteco.mixtecnotes.di.scope.ActivityScope
import org.mixteco.mixtecnotes.quiz.QuizActivity

@ActivityScope
@Subcomponent(modules = [QuizActivityModule::class])
interface QuizActivityComponent {

    fun injectTo(activity: QuizActivity)
}