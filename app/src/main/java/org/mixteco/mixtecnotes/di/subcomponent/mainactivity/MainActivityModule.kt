package org.mixteco.mixtecnotes.di.subcomponent.mainactivity

import dagger.Module
import dagger.Provides
import org.mixteco.mixtecnotes.di.ActivityModule
import org.mixteco.mixtecnotes.di.scope.ActivityScope
import org.mixteco.mixtecnotes.home.MainActivity
import org.mixteco.mixtecnotes.home.MainActivityPresenter
import org.mixteco.mixtecnotes.home.MainActivityView
import org.mixteco.mixtecnotes.model.DataRepository

@Module
class MainActivityModule(activity: MainActivity) : ActivityModule(activity) {

    @Provides
    @ActivityScope
    fun provideMainActivityView(): MainActivityView = activity as MainActivityView

    @Provides
    @ActivityScope
    fun provideMainActivityPresenter(dataRepository: DataRepository) : MainActivityPresenter = MainActivityPresenter(dataRepository)

}