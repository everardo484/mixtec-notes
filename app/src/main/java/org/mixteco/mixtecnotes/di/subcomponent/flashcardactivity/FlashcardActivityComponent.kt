package org.mixteco.mixtecnotes.di.subcomponent.flashcardactivity

import dagger.Subcomponent
import org.mixteco.mixtecnotes.di.scope.ActivityScope
import org.mixteco.mixtecnotes.flashcard.FlashcardActivity

@ActivityScope
@Subcomponent(modules = [FlashcardActivityModule::class])
interface FlashcardActivityComponent {

    fun injectTo(activity: FlashcardActivity)
}