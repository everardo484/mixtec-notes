package org.mixteco.mixtecnotes.di.subcomponent.flashcardactivity

import dagger.Module
import dagger.Provides
import org.mixteco.mixtecnotes.di.ActivityModule
import org.mixteco.mixtecnotes.di.scope.ActivityScope
import org.mixteco.mixtecnotes.flashcard.FlashcardActivity
import org.mixteco.mixtecnotes.flashcard.FlashcardActivityPresenter
import org.mixteco.mixtecnotes.flashcard.FlashcardActivityView
import org.mixteco.mixtecnotes.model.DataRepository

@Module
class FlashcardActivityModule(activity: FlashcardActivity) : ActivityModule(activity) {

    @ActivityScope
    @Provides
    fun provideFlashcardView(): FlashcardActivityView = activity as FlashcardActivityView

    @ActivityScope
    @Provides
    fun providesFlashCardPresenter(dataRepository: DataRepository): FlashcardActivityPresenter =
            FlashcardActivityPresenter(dataRepository)
}