package org.mixteco.mixtecnotes.di.subcomponent.mainactivity

import dagger.Subcomponent
import org.mixteco.mixtecnotes.di.scope.ActivityScope
import org.mixteco.mixtecnotes.home.MainActivity

@ActivityScope
@Subcomponent(modules = [(MainActivityModule::class)])
interface MainActivityComponent {

    fun injectTo(activity: MainActivity)
}