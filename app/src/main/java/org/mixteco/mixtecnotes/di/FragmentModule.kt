package org.mixteco.mixtecnotes.di

import android.support.v4.app.Fragment
import dagger.Module
import dagger.Provides
import org.mixteco.mixtecnotes.di.scope.FragmentScope

@Module
abstract class FragmentModule(protected val fragment: Fragment) {

    @Provides
    @FragmentScope
    fun provideFragment(): Fragment = fragment

}