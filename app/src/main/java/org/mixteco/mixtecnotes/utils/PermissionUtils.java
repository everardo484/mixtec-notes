package org.mixteco.mixtecnotes.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

public final class PermissionUtils {
    private static final String TAG = PermissionUtils.class.getSimpleName();
    public static final int REQUEST_RECORD_AUDIO_AND_WRITE_EXTERNAL_STORAGE_PERMISSION = 207;

    public static void requestRecordAudioAndWriteExternalStoragePermission(@NonNull Activity activity) {
        requestPermission(activity,
                new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_RECORD_AUDIO_AND_WRITE_EXTERNAL_STORAGE_PERMISSION);
    }

    private static void requestPermission(@NonNull Activity activity,
                                          @NonNull String[] permissions,
                                          @IntRange(from = 0) int requestCode) {
        ActivityCompat.requestPermissions(activity, permissions, requestCode);
    }

    private static boolean hasPermission(@NonNull Context context,
                                         @NonNull String permission) {
        return ActivityCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean hasRecordAudioPermission(@NonNull Context context) {
        return hasPermission(context, Manifest.permission.RECORD_AUDIO);
    }

    public static boolean hasWriteExternalStoragePermission(@NonNull Context context) {
        return hasPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    private PermissionUtils() {
    }
}
