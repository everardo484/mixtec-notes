package org.mixteco.mixtecnotes.utils;

public interface Config {
    String NOTE = "NOTE";
    String EMPTY = "EMPTY";
    String PART_OF_SPEECH = "POS";
    String NOTE_CATEGORY = "CATEGORY";
    String NOTE_ENGLISH = "ENGLISH";
    String NOTE_MIXTEC = "MIXTEC";
    String NOTE_REGION = "REGION";
    String NOTE_AUDIO = "AUDIO";
    String NOTE_AUTHOR = "AUTHOR";

    interface Quiz {
        String TOTAL_GUESSES = "total_guesses";
    }

    interface Fragment {
        String CATEGORY = "category";
    }

    interface BuildConfig {
        String CURRENT_VERSION = "2.1.9";
        String STORE_URL = "https://play.google.com/store/apps/details?id=org.mixteco.mixtecnotes";
        boolean UPDATE_REQUIRED = false;
    }
}