package org.mixteco.mixtecnotes.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.auth.FirebaseAuth;

import org.mixteco.mixtecnotes.MixtecNotesApp;

import hugo.weaving.DebugLog;

public final class SharedPrefUtils {
    private static final String PREFERENCES_NAME = "notes_preferences";
    private static final String LOGIN_STATUS = "login_status";

    private static SharedPreferences getSharedPreferences() {
        return MixtecNotesApp.getAppContext().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    @DebugLog
    public static void clearSharedPreferences() {
        getSharedPreferences().edit().clear().apply();
    }

    public static void setLoginStatus(boolean status) {
        getSharedPreferences().getBoolean(LOGIN_STATUS, status);
    }

    public static boolean isLoggedIn() {
        return getSharedPreferences().getBoolean(LOGIN_STATUS, FirebaseAuth.getInstance().getCurrentUser() != null);
    }

    private SharedPrefUtils() { }
}