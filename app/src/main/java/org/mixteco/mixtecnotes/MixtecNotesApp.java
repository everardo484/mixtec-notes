package org.mixteco.mixtecnotes;

import android.app.Application;
import android.content.Context;
import android.os.StrictMode;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import org.mixteco.mixtecnotes.utils.Config;
import org.mixteco.mixtecnotes.di.ApplicationComponent;
import org.mixteco.mixtecnotes.di.ApplicationModule;
import org.mixteco.mixtecnotes.di.DaggerApplicationComponent;
import org.mixteco.mixtecnotes.utils.ForceUpdateChecker;

import java.util.HashMap;
import java.util.Map;

public class MixtecNotesApp extends Application {
    private static volatile Context mApplicationContext;
    public static ApplicationComponent graph;

    public static Context getAppContext() {
        return mApplicationContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mApplicationContext = getApplicationContext();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        initializeDagger();
        checkForUpdate();
    }

    private void initializeDagger() {
        graph = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    private void checkForUpdate() {
        final FirebaseRemoteConfig firebaseRemoteConfig = getRemoteConfigDefaults();
        firebaseRemoteConfig.fetch(60) // fetch every minutes
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        firebaseRemoteConfig.activateFetched();
                    }
                });
    }

    private FirebaseRemoteConfig getRemoteConfigDefaults() {
        final FirebaseRemoteConfig firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        Map<String, Object> remoteConfigDefaults = new HashMap<>();
        remoteConfigDefaults.put(ForceUpdateChecker.KEY_UPDATE_REQUIRED, Config.BuildConfig.UPDATE_REQUIRED);
        remoteConfigDefaults.put(ForceUpdateChecker.KEY_CURRENT_VERSION, Config.BuildConfig.CURRENT_VERSION);
        remoteConfigDefaults.put(ForceUpdateChecker.KEY_UPDATE_URL, Config.BuildConfig.STORE_URL);
        firebaseRemoteConfig.setDefaults(remoteConfigDefaults);

        return firebaseRemoteConfig;
    }
}