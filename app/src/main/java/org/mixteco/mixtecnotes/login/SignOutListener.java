package org.mixteco.mixtecnotes.login;

public interface SignOutListener {

    void onLoggedOutSuccessfully();

    void onLoggedOutFailed();
}