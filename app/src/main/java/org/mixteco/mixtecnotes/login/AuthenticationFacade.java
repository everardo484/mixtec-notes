package org.mixteco.mixtecnotes.login;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.mixteco.mixtecnotes.R;
import org.mixteco.mixtecnotes.utils.Config;
import org.mixteco.mixtecnotes.utils.SharedPrefUtils;

import java.util.Arrays;
import java.util.List;

public class AuthenticationFacade {
    public static final int RC_SIGN_IN = 123;
    private FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
    private final AppCompatActivity mActivity;

    public AuthenticationFacade(@NonNull AppCompatActivity activity) {
        mActivity = activity;
    }

    public void addAuthStateListener(@NonNull FirebaseAuth.AuthStateListener listener) {
        mFirebaseAuth.addAuthStateListener(listener);
    }

    public void removeAuthStateListener(@NonNull FirebaseAuth.AuthStateListener listener) {
        mFirebaseAuth.removeAuthStateListener(listener);
    }

    public boolean isLoggedIn() {
        return SharedPrefUtils.isLoggedIn();
    }

    public void navigateToLoginTemplate() {
        mActivity.startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setLogo(R.mipmap.ic_launcher)
                        .setAvailableProviders(getAuthProviders())
                        .build(), RC_SIGN_IN);
    }

    private List<AuthUI.IdpConfig> getAuthProviders() {
        return Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build(),
                new AuthUI.IdpConfig.GoogleBuilder().build(),
                new AuthUI.IdpConfig.FacebookBuilder().build());
    }

    public void signOut(@NonNull SignOutListener listener) {
        AuthUI.getInstance()
                .signOut(mActivity)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        listener.onLoggedOutSuccessfully();
                    } else {
                        listener.onLoggedOutFailed();
                    }
                });
    }

    @NonNull
    public String getUserName() {
        if (isLoggedIn()) {
            FirebaseUser author = mFirebaseAuth.getCurrentUser();
            if (author != null) {
                if (author.getDisplayName() != null) {
                    return author.getDisplayName();
                }
                String email = author.getEmail();
                if (email != null) {
                    return email.substring(0, email.indexOf("@"));
                }
            }
        }
        return Config.EMPTY;
    }


    public void updateLoginStatus() {
        SharedPrefUtils.setLoginStatus(FirebaseAuth.getInstance() != null);
    }
}
