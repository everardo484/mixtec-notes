package org.mixteco.mixtecnotes.add;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import org.mixteco.mixtecnotes.R;
import org.mixteco.mixtecnotes.adapter.Category;
import org.mixteco.mixtecnotes.adapter.SpinnerArrayAdapter;
import org.mixteco.mixtecnotes.common.BaseFragment;
import org.mixteco.mixtecnotes.utils.Config;
import org.mixteco.mixtecnotes.di.ApplicationComponent;
import org.mixteco.mixtecnotes.di.subcomponent.addnotefragment.AddNoteFragmentModule;
import org.mixteco.mixtecnotes.model.Note;
import org.mixteco.mixtecnotes.utils.PermissionUtils;
import org.parceler.Parcels;

import java.io.IOException;
import java.util.Arrays;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cafe.adriel.androidaudiorecorder.AndroidAudioRecorder;
import cafe.adriel.androidaudiorecorder.model.AudioSampleRate;
import cafe.adriel.androidaudiorecorder.model.AudioSource;
import hugo.weaving.DebugLog;

import static android.app.Activity.RESULT_OK;
import static org.mixteco.mixtecnotes.utils.Config.NOTE_AUDIO;
import static org.mixteco.mixtecnotes.utils.Config.NOTE_AUTHOR;
import static org.mixteco.mixtecnotes.utils.Config.NOTE_CATEGORY;
import static org.mixteco.mixtecnotes.utils.Config.NOTE_ENGLISH;
import static org.mixteco.mixtecnotes.utils.Config.NOTE_MIXTEC;
import static org.mixteco.mixtecnotes.utils.Config.PART_OF_SPEECH;
import static org.mixteco.mixtecnotes.utils.Config.NOTE_REGION;
import static org.mixteco.mixtecnotes.utils.Config.EMPTY;
import static org.mixteco.mixtecnotes.utils.Config.NOTE;

public class AddNoteFragment extends BaseFragment<AddNoteFragmentView, AddNoteFragmentPresenter>
        implements AddNoteFragmentView,
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnCompletionListener,
        AdapterView.OnItemSelectedListener,
        BottomNavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = AddNoteFragment.class.getSimpleName();

    @BindView(R.id.englishTextInputLayout)
    TextInputLayout englishWord;

    @BindView(R.id.mixtecTextInputLayout)
    TextInputLayout mixtecTranslation;

    @BindView(R.id.regionTextInputLayout)
    TextInputLayout region;

    @BindView(R.id.posSpinner)
    Spinner partOfSpeechSpinner;

    @BindView(R.id.newEditView)
    View mView;

    @Inject
    MediaPlayer mediaPlayer;

    @Inject
    AddNoteFragmentPresenter presenter;

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigation;

    private Unbinder binder;

    private ProgressDialog mProgress;
    private String mCurrentAudioFilePath = EMPTY;
    private String mPreviousAudioFilePath = EMPTY;
    private String mPartOfSpeech = EMPTY;
    private String mCategory;
    private Note updateNote = null;
    private volatile boolean isSavedSuccessfully = false;

    private AddNoteActivityView hostActivityView;

    private static final int REQUEST_RECORD_AUDIO_CODE = 201;

    public static AddNoteFragment newInstance(@NonNull Bundle args) {
        final AddNoteFragment fragment = new AddNoteFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        hostActivityView = (AddNoteActivityView) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        final Bundle args = getArguments();
        if (args == null) {
            throw new IllegalArgumentException(getResources().getString(R.string.fragIllegalArg));
        }

        mCategory = args.getString(Config.Fragment.CATEGORY);
        updateNote = Parcels.unwrap(args.getParcelable(NOTE));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_note, container, false);
        binder = ButterKnife.bind(this, view);
        bottomNavigation.setOnNavigationItemSelectedListener(this);

        updateTitleIfNeeded();
        setUpTextChangedListener();
        setUpSpinnerAdapter();
        requestRecordAudioPermissionIfNeeded();
        handleUpdateStateIfNeeded();
        setUpMediaPlayerListener();

        mProgress = new ProgressDialog(getActivity());

        return view;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.bottom_nav_record_audio:
                handleOnRecordAudioClicked();
                break;
            case R.id.bottom_nav_playback:
                handleOnPlayBackClicked();
                break;
            case R.id.bottom_nav_save:
                handleOnSaveButtonClicked();
                break;
        }
        return true;
    }

    @NonNull
    @Override
    public AddNoteFragmentPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void onAudioAddedSuccess(Uri uri) {
        mProgress.dismiss();
        mCurrentAudioFilePath = uri.toString();
    }

    @Override
    public void onAudioAddedFailure() {
        mCurrentAudioFilePath = EMPTY;
    }

    @Override
    public void deletePreviousAudioFileSafely() {
        if (isSafeToDeletePreviousFile()) {
            presenter.deleteAudioFilePath(mPreviousAudioFilePath);
        }
    }

    @Override
    public void showMessage(@StringRes int msg) {
        Snackbar.make(mView, msg, Snackbar.LENGTH_SHORT).setAction(getString(R.string
                .snackbar_action), null).show();
    }

    @Override
    public void showToastMessage(@StringRes int msg) {
        Toast.makeText(getActivity(), getString(msg), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void startRecording() {
        AndroidAudioRecorder.with(this)
                .setFilePath(presenter.getAudioFileName())
                .setColor(getResources().getColor(R.color.colorPrimaryDark))
                .setRequestCode(REQUEST_RECORD_AUDIO_CODE)
                .setSource(AudioSource.MIC)
                .setSampleRate(AudioSampleRate.HZ_48000)
                .setAutoStart(true)
                .setKeepDisplayOn(true)
                .recordFromFragment();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapter, View view, int position, long id) {
        if (position == 0) {
            mPartOfSpeech = EMPTY;
        } else {
            mPartOfSpeech = adapter.getItemAtPosition(position).toString();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        mPartOfSpeech = EMPTY;
    }

    public void handleOnSaveButtonClicked() {
        saveNote();
    }

    @Override
    public void navigateBack() {
        hostActivityView.onNavigateBack();
    }

    public void handleOnRecordAudioClicked() {
        startRecording();
    }

    private boolean isCurrentAudioFilePathEmpty() {
        return mCurrentAudioFilePath.equalsIgnoreCase(EMPTY);
    }

    private boolean isPreviousAudioFilePathEmpty() {
        return mPreviousAudioFilePath.equalsIgnoreCase(EMPTY);
    }

    @DebugLog
    private void saveNote() {
        if (isAudioPathValidated()) {
            presenter.post(getNoteFrom(getContentValues()));
        } else {
            hostActivityView.showMessage(R.string.audio_required);
        }
    }

    @DebugLog
    private boolean isAudioPathValidated() {
        if (isUpdateState() && !isPreviousAudioFilePathEmpty()) {
            return true;
        }

        return !isCurrentAudioFilePathEmpty();
    }

    @Override
    public void onNoteSavedSuccessfully() {
        isSavedSuccessfully = true;
    }

    @Override
    public void onNoteSavedFailure() {
        isSavedSuccessfully = false;
    }

    private void updateTitleIfNeeded() {
        if (isUpdateState()) {
            hostActivityView.setEditTitle();
        }
    }

    @Override
    public void loadEditData() {
        englishWord.getEditText().setText(updateNote.getEnglishWord());
        mixtecTranslation.getEditText().setText(updateNote.getMixtecTranslation());
        region.getEditText().setText(updateNote.getRegion());
        mPartOfSpeech = updateNote.getPos();
        mPreviousAudioFilePath = updateNote.getAudio();
        partOfSpeechSpinner.setSelection(getPartOfSpeechIndexOf(mPartOfSpeech));
    }

    private void uploadAudio() {
        mProgress.setMessage(getString(R.string.audio_upload));
        mProgress.show();
        presenter.pushAudioFile(Category.valueOf(mCategory));
    }

    @Override
    public void updateSaveButtonState() {
        if (hasRequiredFields() && !isCurrentAudioFilePathEmpty()) {
            bottomNavigation.findViewById(R.id.bottom_nav_save).setVisibility(View.VISIBLE);
        } else {
            bottomNavigation.findViewById(R.id.bottom_nav_save).setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void updateAudioButtonState() {
        if (hasRequiredFields() && hasRequiredPermissions()) {
            bottomNavigation.findViewById(R.id.bottom_nav_record_audio).setVisibility(View.VISIBLE);
        } else {
            bottomNavigation.findViewById(R.id.bottom_nav_record_audio).setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void updateFooterViewState() {
        if (hasRequiredFields()) {
            bottomNavigation.setVisibility(View.VISIBLE);
        } else {
            bottomNavigation.setVisibility(View.GONE);
        }
    }

    private void handleUpdateStateIfNeeded() {
        if (isUpdateState()) {
            loadEditData();
            updateFooterViewState();
            updateSaveButtonState();
            updateAudioButtonState();
        }
    }

    @DebugLog
    private boolean isUpdateState() {
        return updateNote != null;
    }

    private boolean isSafeToDeletePreviousFile() {
        return !(isCurrentAudioFilePathEmpty() || isPreviousAudioFilePathEmpty()) &&
                !(mPreviousAudioFilePath.equalsIgnoreCase(mCurrentAudioFilePath));
    }

    private int getPartOfSpeechIndexOf(String partOfSpeech) {
        return Arrays.asList(getResources()
                .getStringArray(R.array.part_of_speech))
                .indexOf(partOfSpeech);
    }


    @DebugLog
    private boolean hasRequiredFields() {
        String englishText = englishWord.getEditText().getText().toString();
        String mixtecText = mixtecTranslation.getEditText().getText().toString();
        String regionText = region.getEditText().getText().toString();
        return englishText.trim().length() != 0 && mixtecText.trim().length() != 0 && regionText.trim().length() != 0;
    }

    private void requestRecordAudioPermissionIfNeeded() {
        if (!(hasRequiredPermissions())) {
            PermissionUtils.requestRecordAudioAndWriteExternalStoragePermission(getActivity());
        }
    }

    @Override
    public void enablePlayAudioBackButton() {
        bottomNavigation.findViewById(R.id.bottom_nav_playback).setVisibility(View.VISIBLE);
    }

    @Override
    public void disablePlayAudioBackButton() {
        bottomNavigation.findViewById(R.id.bottom_nav_playback).setVisibility(View.INVISIBLE);
    }

    private void updatePlayAudioBackButtonDrawable(@DrawableRes int drawableResId) {
        bottomNavigation.getMenu().findItem(R.id.bottom_nav_playback).setIcon(drawableResId);
    }

    public void handleOnPlayBackClicked() {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            updatePlayAudioBackButtonDrawable(R.drawable.ic_play_circle_24dp);
        } else {
            playAudio();
        }
    }

    @Override
    public void playAudio() {
        mediaPlayer.reset();
        try {
            mediaPlayer.setDataSource(mCurrentAudioFilePath);
            mediaPlayer.prepareAsync();
        } catch (IOException e) {
            Log.e(TAG, "prepare() failed");
        }
    }

    private void updatePlayBackButtonState() {
        if (isCurrentAudioFilePathEmpty() && bottomNavigation.isShown()) {
            disablePlayAudioBackButton();
        } else {
            enablePlayAudioBackButton();
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        updatePlayAudioBackButtonDrawable(R.drawable.ic_pause_circle_24dp);
        mediaPlayer.start();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        stopMediaPlayer();
        updatePlayAudioBackButtonDrawable(R.drawable.ic_play_circle_24dp);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_RECORD_AUDIO_CODE) {
            if (resultCode == RESULT_OK) {
                uploadAudio();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PermissionUtils.REQUEST_RECORD_AUDIO_AND_WRITE_EXTERNAL_STORAGE_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                updateAudioButtonState();
            } else {
                showMessage(R.string.recording_permission_denied);
            }
        } else {
            Log.d(TAG, "Recording permission request was cancelled");
        }
    }


    @Override
    public void onDestroy() {
        if (isCurrentAudioFilePathDirty()) {
            presenter.deleteAudioFilePath(mCurrentAudioFilePath);
        } else {
            deletePreviousAudioFileSafely();
        }

        stopMediaPlayer();

        super.onDestroy();
    }

    @Override
    public void stopMediaPlayer() {
        mediaPlayer.stop();
        mediaPlayer.reset();
    }

    private boolean isCurrentAudioFilePathDirty() {
        return !isSavedSuccessfully && !isCurrentAudioFilePathEmpty();
    }

    private boolean hasRequiredPermissions() {
        return PermissionUtils.hasRecordAudioPermission(getActivity()) &&
                PermissionUtils.hasWriteExternalStoragePermission(getActivity());
    }

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new AddNoteFragmentModule(this)).injectTo(this);
    }

    private void setUpMediaPlayerListener() {
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnCompletionListener(this);
    }

    private void setUpTextChangedListener() {
        englishWord.getEditText().addTextChangedListener(mTextChangedListener);
        mixtecTranslation.getEditText().addTextChangedListener(mTextChangedListener);
        region.getEditText().addTextChangedListener(mTextChangedListener);
    }

    private void setUpSpinnerAdapter() {
        SpinnerArrayAdapter<String> dataAdapter = new SpinnerArrayAdapter<>(
                getContext(),
                android.R.layout.simple_spinner_item,
                Arrays.asList(getResources().getStringArray(R.array.part_of_speech)));

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        partOfSpeechSpinner.setAdapter(dataAdapter);
        partOfSpeechSpinner.setOnItemSelectedListener(this);
    }

    private ContentValues getContentValues() {
        final ContentValues contentValues = new ContentValues();
        contentValues.put(NOTE_ENGLISH, englishWord.getEditText().getText().toString());
        contentValues.put(NOTE_MIXTEC, mixtecTranslation.getEditText().getText().toString());
        contentValues.put(NOTE_REGION, region.getEditText().getText().toString());
        contentValues.put(PART_OF_SPEECH, mPartOfSpeech);
        contentValues.put(NOTE_AUTHOR, hostActivityView.getAuthor());
        contentValues.put(NOTE_CATEGORY, mCategory);
        contentValues.put(NOTE_AUDIO, (isUpdateState() && isCurrentAudioFilePathEmpty()) ? mPreviousAudioFilePath :
                mCurrentAudioFilePath);
        return contentValues;
    }


    private Note getNoteFrom(@NonNull ContentValues values) {
        Note note = new Note();
        if (isUpdateState()) {
            note.setKey(updateNote.getKey());
        }
        note.englishWord = values.getAsString(Config.NOTE_ENGLISH);
        note.mixtecTranslation = values.getAsString(Config.NOTE_MIXTEC);
        note.pos = values.getAsString(Config.PART_OF_SPEECH);
        note.category = values.getAsString(Config.NOTE_CATEGORY);
        note.region = values.getAsString(Config.NOTE_REGION);
        note.audio = values.getAsString(Config.NOTE_AUDIO);
        note.author = values.getAsString(Config.NOTE_AUTHOR);
        return note;
    }

    private final TextWatcher mTextChangedListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            updateFooterViewState();
            updateAudioButtonState();
            updatePlayBackButtonState();
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binder.unbind();
    }
}