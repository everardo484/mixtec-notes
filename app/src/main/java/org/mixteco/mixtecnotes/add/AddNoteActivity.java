package org.mixteco.mixtecnotes.add;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.widget.FrameLayout;

import org.mixteco.mixtecnotes.R;
import org.mixteco.mixtecnotes.common.BaseActivity;
import org.mixteco.mixtecnotes.di.ApplicationComponent;
import org.mixteco.mixtecnotes.di.subcomponent.addnoteactivity.AddNoteActivityModule;
import org.mixteco.mixtecnotes.utils.Config;
import org.parceler.Parcels;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import hugo.weaving.DebugLog;

import static org.mixteco.mixtecnotes.utils.Config.NOTE;

public class AddNoteActivity extends BaseActivity<AddNoteActivityView, AddNoteActivityPresenter>
        implements AddNoteActivityView {

    @Inject
    AddNoteActivityPresenter presenter;

    @BindView(R.id.fragment_container)
    FrameLayout fragmentContainer;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);
        ButterKnife.bind(this);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            Bundle args = new Bundle();
            args.putString(Config.Fragment.CATEGORY, getCategory());
            args.putParcelable(NOTE, Parcels.wrap(Parcels.unwrap(getIntent().getExtras().getParcelable(NOTE))));
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, AddNoteFragment.newInstance(args))
                    .commit();
        }
    }

    @DebugLog
    private String getCategory() {
        return getIntent().getExtras().getString(Config.Fragment.CATEGORY);
    }

    @Override
    public void setEditTitle() {
        setTitle(getString(R.string.edit_mixtec_note));
    }

    @NonNull
    @Override
    public String getAuthor() {
        return super.getAuthor();
    }

    @Override
    public void onNavigateBack() {
        super.onBackPressed();
    }

    @Override
    public void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new AddNoteActivityModule(this)).injectTo(this);
    }

    @Override
    public void showMessage(@StringRes int msg) {
        Snackbar.make(fragmentContainer, getString(msg), Snackbar.LENGTH_SHORT).setAction(getString(R.string
                .snackbar_action), null).show();
    }

    @NonNull
    @Override
    public AddNoteActivityPresenter getPresenter() {
        return presenter;
    }
}