package org.mixteco.mixtecnotes.add;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import org.mixteco.mixtecnotes.mvp.MvpView;

public interface AddNoteActivityView extends MvpView {

    void onNavigateBack();

    void setEditTitle();

    @NonNull String getAuthor();

    void showMessage(@StringRes int msg);
}
