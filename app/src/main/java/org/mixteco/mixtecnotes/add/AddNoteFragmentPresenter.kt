package org.mixteco.mixtecnotes.add

import android.net.Uri
import android.os.Environment
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.mixteco.mixtecnotes.R
import org.mixteco.mixtecnotes.adapter.Category
import org.mixteco.mixtecnotes.model.DataRepository
import org.mixteco.mixtecnotes.model.Note
import org.mixteco.mixtecnotes.mvp.AbstractMvpPresenter
import java.io.File
import java.util.*

class AddNoteFragmentPresenter(private val dataRepository: DataRepository) : AbstractMvpPresenter<AddNoteFragmentView>() {
    private val AUDIO_EXTENSION = ".wav"
    private var mFileName: String = Environment.getExternalStorageDirectory().absolutePath
    private val disposable = CompositeDisposable()

    fun post(note: Note) {
        disposable.add(dataRepository.post(note)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError { e -> noteSavedFailure(e) }
                .doOnComplete { noteSavedSuccessfully() }
                .subscribe())
    }

    private fun noteSavedSuccessfully() {
        ifViewAttached { view -> view.onNoteSavedSuccessfully() }
        ifViewAttached { view -> view.stopMediaPlayer() }
        ifViewAttached { view -> view.navigateBack() }
    }

    private fun noteSavedFailure(throwable: Throwable) {
        ifViewAttached { view -> view.showMessage(R.string.push_note_failed) }
        ifViewAttached { view -> view.onNoteSavedFailure() }
        ifViewAttached { view -> view.stopMediaPlayer() }
        ifViewAttached { view -> view.navigateBack() }
        Log.d("BACKEND", throwable.message)
    }

    fun pushAudioFile(category: Category) {
        val audioFile = Uri.fromFile(File(mFileName))
        disposable.add(dataRepository.putAudioFile(audioFile, category)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError { onAudioFailed() }
                .doOnComplete { onAudioUploadedSuccess(audioFile, category) }
                .subscribe())
    }

    private fun onAudioUploadedSuccess(uri: Uri, category: Category) {
        disposable.add(dataRepository.getDownloadUri(uri, category)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError { onAudioFailed() }
                .doOnSuccess { downloadUri -> onAudioDownloadUriSuccessfully(downloadUri) }
                .subscribe())
    }

    private fun onAudioDownloadUriSuccessfully(uri: Uri) {
        ifViewAttached { view -> view.onAudioAddedSuccess(uri) }
        ifViewAttached { view -> view.showMessage(R.string.audio_upload_success) }
        ifViewAttached { view -> view.enablePlayAudioBackButton() }
        ifViewAttached { view -> view.updateSaveButtonState() }
    }

    private fun onAudioFailed() {
        ifViewAttached { view -> view.onAudioAddedFailure() }
        ifViewAttached { view -> view.showMessage(R.string.audio_failed) }
        ifViewAttached { view -> view.disablePlayAudioBackButton() }
    }

    fun getAudioFileName(): String {
        return mFileName
    }

    fun deleteAudioFilePath(audioPath: String) {
        disposable.add(dataRepository.deleteAudioFile(Uri.parse(audioPath))
                .observeOn(Schedulers.io())
                .doOnError { e -> Log.d("BACKEND", "Unable to onDelete Audio File ${e.message}") }
                .doOnComplete { Log.d("BACKEND", "Audio File was successfully deleted") }
                .subscribe())
    }

    override fun onDestroy() {
        disposable.clear()
        super.onDestroy()
    }

    init {
        mFileName += "/" + (UUID.randomUUID().toString() + AUDIO_EXTENSION)
    }
}