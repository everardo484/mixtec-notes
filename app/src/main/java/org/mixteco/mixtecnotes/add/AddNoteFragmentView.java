package org.mixteco.mixtecnotes.add;

import android.net.Uri;
import android.support.annotation.StringRes;

import org.mixteco.mixtecnotes.mvp.MvpView;

public interface AddNoteFragmentView extends MvpView {

    void onAudioAddedSuccess(Uri uri);

    void showMessage(@StringRes int msg);

    void showToastMessage(@StringRes int msg);

    void onAudioAddedFailure();

    void startRecording();

    void loadEditData();

    void updateSaveButtonState();

    void updateAudioButtonState();

    void updateFooterViewState();

    void onNoteSavedSuccessfully();

    void onNoteSavedFailure();

    void deletePreviousAudioFileSafely();

    void playAudio();

    void enablePlayAudioBackButton();

    void disablePlayAudioBackButton();

    void navigateBack();

    void stopMediaPlayer();
}