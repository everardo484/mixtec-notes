package org.mixteco.mixtecnotes.adapter;

import android.support.annotation.StringRes;

import org.mixteco.mixtecnotes.R;

public enum Category {
    NUMBERS(R.string.category_numbers),
    COLORS(R.string.category_colors),
    FAMILY(R.string.category_family),
    ABC(R.string.category_abc);

    private final int mCategoryNameResourceId;

    Category(@StringRes int categoryName) {
        mCategoryNameResourceId = categoryName;
    }

    public int getCategoryNameResourceId () {
        return mCategoryNameResourceId;
    }
}