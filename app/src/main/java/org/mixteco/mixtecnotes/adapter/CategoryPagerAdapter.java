package org.mixteco.mixtecnotes.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import org.mixteco.mixtecnotes.home.MainFragment;

public class CategoryPagerAdapter extends FragmentStatePagerAdapter {

    private final Category[] types = Category.values();

    public CategoryPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return MainFragment.newInstance(types[position]);
    }

    @Override
    public int getCount() {
        return types.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return types[position].name();
    }
}