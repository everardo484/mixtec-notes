package org.mixteco.mixtecnotes.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import org.mixteco.mixtecnotes.R;
import org.mixteco.mixtecnotes.model.Note;

import butterknife.BindView;
import butterknife.ButterKnife;
import hugo.weaving.DebugLog;

public class NotesAdapter extends FirestoreRecyclerAdapter<Note, NotesAdapter.ViewHolder> {

    private Context mContext;
    private NotesAdapter.OnDataChangeListener mListener;
    private NotesAdapter.OnItemLongClickListener mLongClickListener;
    private NotesAdapter.OnPlayAudioListener mPlayAudioListener;

    public NotesAdapter(@NonNull FirestoreRecyclerOptions<Note> options) {
        super(options);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        mContext = parent.getContext();
        return new ViewHolder(itemView);
    }

    @DebugLog
    public boolean isListening() {
        return getSnapshots().isListening(this);
    }

    @DebugLog
    @Override
    public void onDataChanged() {
        super.onDataChanged();
        mListener.OnDataChanged();
    }

    @Override
    protected void onBindViewHolder(ViewHolder holder, int position, Note note) {
        holder.mEnglishWord.setText(note.getEnglishWord());
        holder.mMixtecWord.setText(note.getMixtecTranslation());
        holder.mRegion.setText(note.getRegion());


        if (note.hasAuthor()) {
            holder.mAuthor.setText(mContext.getResources().getString(R.string.by, note.getAuthor()));
            holder.mAuthor.setVisibility(View.VISIBLE);
        } else {
            holder.mAuthor.setVisibility(View.GONE);
        }

        if (note.hasPartOfSpeech()) {
            holder.mPartOfSpeech.setText(note.getPos());
            holder.mPartOfSpeech.setVisibility(View.VISIBLE);
        } else {
            holder.mPartOfSpeech.setVisibility(View.GONE);
        }

        if (note.hasAudio()) {
            holder.mAudioImageView.setVisibility(View.VISIBLE);
            holder.mAudioImageView.setOnClickListener(view -> mPlayAudioListener.onItemPlayAudio(view, note));
        } else {
            holder.mAudioImageView.setVisibility(View.GONE);
        }

        holder.itemView.setOnLongClickListener((View view) -> {
            mLongClickListener.onLongClick(view, note);
            return true;
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.englishTextView)
        TextView mEnglishWord;
        @BindView(R.id.mixtecTranslation)
        TextView mMixtecWord;
        @BindView(R.id.posTextView)
        TextView mPartOfSpeech;
        @BindView(R.id.regionTextView)
        TextView mRegion;
        @BindView(R.id.playAudio)
        ImageView mAudioImageView;
        @BindView(R.id.authorTextView)
        TextView mAuthor;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    public void setOnDataChangeListener(NotesAdapter.OnDataChangeListener listener) {
        mListener = listener;
    }

    public void setOnItemLongClickListener(NotesAdapter.OnItemLongClickListener listener) {
        mLongClickListener = listener;
    }

    public void setOnPlayAudioListener(NotesAdapter.OnPlayAudioListener listener) {
        mPlayAudioListener = listener;
    }

    public interface OnDataChangeListener {
        void OnDataChanged();
    }

    public interface OnPlayAudioListener {
        void onItemPlayAudio(@NonNull View view, @NonNull Note note);
    }

    public interface OnItemLongClickListener {
        void onLongClick(@NonNull View view, @NonNull Note note);
    }
}