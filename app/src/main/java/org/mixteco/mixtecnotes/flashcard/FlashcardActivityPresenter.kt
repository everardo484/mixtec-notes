package org.mixteco.mixtecnotes.flashcard

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.mixteco.mixtecnotes.model.DataRepository
import org.mixteco.mixtecnotes.model.Note
import org.mixteco.mixtecnotes.mvp.AbstractMvpPresenter

class FlashcardActivityPresenter(private val dataRepository: DataRepository) : AbstractMvpPresenter<FlashcardActivityView>() {
    private val disposable = CompositeDisposable()

    fun fetch(type: String) {
        disposable.add(dataRepository.fetch(type)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { ifViewAttached { view -> view.showLoadingProgressBar() } }
                .doOnError { ifViewAttached { view -> view.hideLoadingProgressBar() } }
                .doOnSuccess { notes: List<Note> -> onNotesLoadCompleted(notes) }
                .subscribe())
    }

    private fun onNotesLoadCompleted(notes: List<Note>) {
        ifViewAttached { view -> view.onNotesLoading(notes)}
        ifViewAttached { view -> view.hideLoadingProgressBar() }
        ifViewAttached { view -> view.resetFlashcards() }
    }

    override fun onDestroy() {
        disposable.clear()
        super.onDestroy()
    }
}