package org.mixteco.mixtecnotes.flashcard

import org.mixteco.mixtecnotes.model.Note
import org.mixteco.mixtecnotes.mvp.MvpView

interface FlashcardActivityView: MvpView {
    fun onNotesLoading(notes: List<Note>)
    fun showLoadingProgressBar()
    fun hideLoadingProgressBar()
    fun resetFlashcards()
}