package org.mixteco.mixtecnotes.flashcard

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import android.widget.ProgressBar
import hugo.weaving.DebugLog
import org.mixteco.mixtecnotes.R
import org.mixteco.mixtecnotes.common.BaseActivity
import org.mixteco.mixtecnotes.di.ApplicationComponent
import org.mixteco.mixtecnotes.di.subcomponent.flashcardactivity.FlashcardActivityModule
import org.mixteco.mixtecnotes.model.Note
import org.mixteco.mixtecnotes.utils.Config
import java.util.*
import javax.inject.Inject

class FlashcardActivity : BaseActivity<FlashcardActivityView, FlashcardActivityPresenter>(), FlashcardActivityView,
        ViewPager.OnPageChangeListener {

    private var mPager: ViewPager? = null
    private var mPagerAdapter: PagerAdapter? = null
    var mItems: List<Note> = ArrayList()
    private var progressBar: ProgressBar? = null
    private var flashcardsReset:Boolean = true
    private val reset:String = "reset"
    private lateinit var category: String

    @Inject
    lateinit var mPresenter: FlashcardActivityPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flashcard)

        progressBar = findViewById<View>(R.id.progress_bar) as ProgressBar
        category = getCategory()

        mPager = findViewById<View>(R.id.pager) as ViewPager
        mPagerAdapter = ScreenSlidePagerAdapter(supportFragmentManager)
    }

    @DebugLog
    private fun getCategory() : String {
      return intent.extras.getString(Config.Fragment.CATEGORY)
    }

    override fun onStart() {
        super.onStart()

        if (flashcardsReset) {
            resetFlashcardsOrGetNotesIfEmpty()
            flashcardsReset = false
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putBoolean(reset, flashcardsReset)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        flashcardsReset = savedInstanceState!!.getBoolean(reset)
    }

     override fun resetFlashcards() {
        Collections.shuffle(mItems)
        mPager?.setAdapter(mPagerAdapter)
        mPager?.addOnPageChangeListener(this)
    }


    override fun onNotesLoading(notes: List<Note>) {
        mItems = notes
    }

    override fun showLoadingProgressBar() {
        progressBar?.visibility = View.VISIBLE
    }

    override fun hideLoadingProgressBar() {
        progressBar?.visibility = View.GONE
    }

    override fun onStop() {
        super.onStop()
        mPager?.clearOnPageChangeListeners()
    }

    override fun onPageSelected(position: Int) {
        invalidateOptionsMenu()
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    private fun resetFlashcardsOrGetNotesIfEmpty() {
        if (mItems.isEmpty()) {
            presenter.fetch(category)
        } else {
            resetFlashcards()
        }
    }

    override fun getPresenter(): FlashcardActivityPresenter {
        return mPresenter
    }

    override fun injectDependencies(applicationComponent: ApplicationComponent) {
        applicationComponent.plus(FlashcardActivityModule(this)).injectTo(this)
    }

    private inner class ScreenSlidePagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
        override fun getCount(): Int {
            return mItems.size
        }

        override fun getItem(position: Int): Fragment {
            return PagerFragment.newInstance(mItems[position])
        }
    }
}