package org.mixteco.mixtecnotes.flashcard;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;
import org.mixteco.mixtecnotes.R;
import org.mixteco.mixtecnotes.model.Note;
import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;

import static org.mixteco.mixtecnotes.utils.Config.NOTE;

public class CardFrontFragment extends Fragment {

    @BindView(R.id.cardViewFlashcard)
    CardView mCardView;

    @BindView(R.id.theWord)
    TextView mixtecWord;

    @BindView(R.id.regionTextView)
    TextView region;

    @BindView(R.id.playAudio)
    ImageView mAudio;

    private Note mCurrentNote;

    public static CardFrontFragment newInstance(final @NotNull Bundle args) {
        CardFrontFragment cardFrontFragment = new CardFrontFragment();
        cardFrontFragment.setArguments(args);
        return cardFrontFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        final Bundle args = getArguments();
        if (args == null) {
            throw new IllegalArgumentException(getResources().getString(R.string.fragIllegalArg));
        }

        mCurrentNote = Parcels.unwrap(args.getParcelable(NOTE));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_flashcard, container, false);
        ButterKnife.bind(this, view);

        mCardView.setOnClickListener(((PagerFragment) getParentFragment()));
        mixtecWord.setText(mCurrentNote.getMixtecTranslation());
        region.setText(mCurrentNote.getRegion());

        if (mCurrentNote.hasAudio()) {
            mAudio.setVisibility(View.VISIBLE);
            mAudio.setOnClickListener(((PagerFragment) getParentFragment()));
        } else {
            mAudio.setVisibility(View.GONE);
        }

        return view;
    }
}
