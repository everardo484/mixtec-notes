package org.mixteco.mixtecnotes.flashcard;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.mixteco.mixtecnotes.R;
import org.mixteco.mixtecnotes.model.Note;
import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;

import static org.mixteco.mixtecnotes.utils.Config.NOTE;

public class CardBackFragment extends Fragment {

    @BindView(R.id.cardViewFlashcard)
    CardView mCardView;

    @BindView(R.id.theWord)
    TextView englishWord;

    @BindView(R.id.regionTextView)
    TextView region;

    @BindView(R.id.playAudio)
    ImageView mAudio;

    private Note mCurrentNote;

    public static CardBackFragment newInstance(final @NonNull Bundle args) {
        CardBackFragment cardBackFragment = new CardBackFragment();
        cardBackFragment.setArguments(args);
        return cardBackFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        final Bundle args = getArguments();
        if (args == null) {
            throw new IllegalArgumentException(getResources().getString(R.string.fragIllegalArg));
        }

        mCurrentNote = Parcels.unwrap(args.getParcelable(NOTE));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_flashcard, container, false);
        ButterKnife.bind(this, view);

        mCardView.setOnClickListener(((PagerFragment) getParentFragment()));
        englishWord.setText(mCurrentNote.getEnglishWord());
        region.setVisibility(View.GONE);
        mAudio.setVisibility(View.GONE);

        return view;
    }
}
