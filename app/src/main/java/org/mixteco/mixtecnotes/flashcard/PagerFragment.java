package org.mixteco.mixtecnotes.flashcard;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import org.mixteco.mixtecnotes.R;
import org.mixteco.mixtecnotes.common.BaseFragment;
import org.mixteco.mixtecnotes.di.ApplicationComponent;
import org.mixteco.mixtecnotes.di.subcomponent.flashcardfragment.PagerFragmentModule;
import org.mixteco.mixtecnotes.model.Note;
import org.parceler.Parcels;

import java.io.IOException;

import javax.inject.Inject;

import static org.mixteco.mixtecnotes.utils.Config.NOTE;

public class PagerFragment extends BaseFragment<PagerFragmentView, PagerFragmentPresenter> implements PagerFragmentView,
        FragmentManager.OnBackStackChangedListener,
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnCompletionListener,
        View.OnClickListener {

    private Note mCurrentNote;
    private boolean mShowingBack = false;
    private Animation bounce;

    @Inject
    MediaPlayer mediaPlayer;

    @Inject
    PagerFragmentPresenter presenter;

    public static PagerFragment newInstance(Note note) {
        PagerFragment fragment = new PagerFragment();
        Bundle args = new Bundle();
        args.putParcelable(NOTE, Parcels.wrap(note));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        bounce = AnimationUtils.loadAnimation(getContext(), R.anim.bounce);
        mCurrentNote = Parcels.unwrap(getArguments().getParcelable(NOTE));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_pager, container, false);

        handleCurrentState(savedInstanceState);

        getChildFragmentManager().addOnBackStackChangedListener(this);

        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnCompletionListener(this);

        return view;
    }

    private void handleCurrentState(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            createNoteFrontCard();
        } else {
            mShowingBack = getChildFragmentManager().getBackStackEntryCount() > 0;
        }
    }

    public void flipCard() {
        if (mShowingBack) {
            getChildFragmentManager().popBackStack();
        } else {
            createNoteBackCard();
            mShowingBack = true;
        }
    }

    @Override
    public void onBackStackChanged() {
        mShowingBack = getChildFragmentManager().getBackStackEntryCount() > 0;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cardViewFlashcard:
                flipCard();
                break;
            case R.id.playAudio:
                view.startAnimation(bounce);
                playAudio(mCurrentNote);
                break;
        }
    }

    public void playAudio(final Note note) {
        mediaPlayer.reset();
        try {
            mediaPlayer.setDataSource(note.getAudio());
            mediaPlayer.prepareAsync();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mediaPlayer.start();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        bounce.cancel();
        mediaPlayer.stop();
        mediaPlayer.reset();
    }

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new PagerFragmentModule(this)).injectTo(this);
    }

    private void createNoteFrontCard() {
        getChildFragmentManager()
                .beginTransaction()
                .add(R.id.container, CardFrontFragment.newInstance(getCardArguments()))
                .commit();
    }

    private void createNoteBackCard() {
        getChildFragmentManager()
                .beginTransaction()
                .setCustomAnimations(
                        R.anim.card_flip_right_in, R.anim.card_flip_right_out,
                        R.anim.card_flip_left_in, R.anim.card_flip_left_out)
                .replace(R.id.container, CardBackFragment.newInstance(getCardArguments()))
                .addToBackStack(null)
                .commit();
    }

    private Bundle getCardArguments() {
        final Bundle args = new Bundle();
        args.putParcelable(NOTE, Parcels.wrap(mCurrentNote));
        return args;
    }

    public PagerFragment() {
    }

    @NonNull
    @Override
    public PagerFragmentPresenter getPresenter() {
        return presenter;
    }
}
