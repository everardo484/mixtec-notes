package org.mixteco.mixtecnotes.common;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.firebase.auth.FirebaseAuth;

import org.mixteco.mixtecnotes.MixtecNotesApp;
import org.mixteco.mixtecnotes.di.ApplicationComponent;
import org.mixteco.mixtecnotes.login.AuthenticationFacade;
import org.mixteco.mixtecnotes.login.SignOutListener;
import org.mixteco.mixtecnotes.mvp.MvpActivity;
import org.mixteco.mixtecnotes.mvp.MvpPresenter;
import org.mixteco.mixtecnotes.mvp.MvpView;

import hugo.weaving.DebugLog;

public abstract class BaseActivity<V extends MvpView, P extends MvpPresenter<V>> extends MvpActivity<V, P>
        implements FirebaseAuth.AuthStateListener {
    private AuthenticationFacade authFacade;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        injectDependencies(MixtecNotesApp.graph);
        super.onCreate(savedInstanceState);
        authFacade = new AuthenticationFacade(this);
        authFacade.addAuthStateListener(this);
    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        authFacade.updateLoginStatus();
    }

    @DebugLog
    @NonNull
    public String getAuthor() {
        return authFacade.getUserName();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        authFacade.removeAuthStateListener(this);
    }

    protected boolean isLoggedIn() {
        return authFacade.isLoggedIn();
    }

    public void navigateToLoginTemplate() {
        authFacade.navigateToLoginTemplate();
    }

    public void signOut(@NonNull SignOutListener listener) {
        authFacade.signOut(listener);
    }

    protected abstract void injectDependencies(ApplicationComponent applicationComponent);
}