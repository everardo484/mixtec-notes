package org.mixteco.mixtecnotes.common;

import android.os.Bundle;

import org.mixteco.mixtecnotes.MixtecNotesApp;
import org.mixteco.mixtecnotes.di.ApplicationComponent;
import org.mixteco.mixtecnotes.mvp.MvpFragment;
import org.mixteco.mixtecnotes.mvp.MvpPresenter;
import org.mixteco.mixtecnotes.mvp.MvpView;

public abstract class BaseFragment<V extends MvpView, P extends MvpPresenter<V>>  extends MvpFragment<V, P> {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        injectDependencies(MixtecNotesApp.graph);
        super.onCreate(savedInstanceState);
    }

    protected abstract void injectDependencies(ApplicationComponent applicationComponent);
}
