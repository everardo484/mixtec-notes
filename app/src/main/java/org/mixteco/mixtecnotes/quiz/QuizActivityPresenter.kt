package org.mixteco.mixtecnotes.quiz

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.mixteco.mixtecnotes.model.DataRepository
import org.mixteco.mixtecnotes.model.Note
import org.mixteco.mixtecnotes.mvp.AbstractMvpPresenter

class QuizActivityPresenter(private val dataRepository: DataRepository) : AbstractMvpPresenter<QuizActivityView>() {

    private val disposable = CompositeDisposable()

    fun fetch(category: String) {
        disposable.add(dataRepository.fetch(category)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { ifViewAttached { view -> view.showLoadingProgressBar() } }
                .doOnError { ifViewAttached { view -> view.hideLoadingProgressBar() } }
                .doOnSuccess { notes: List<Note> -> onNotesLoadCompleted(notes) }
                .subscribe())
    }

    private fun onNotesLoadCompleted(notes: List<Note>) {
        ifViewAttached { view -> view.onNotesLoading(notes) }
        ifViewAttached { view -> view.hideLoadingProgressBar() }
        ifViewAttached { view -> view.resetQuiz() }
    }

    override fun onDestroy() {
        disposable.clear()
        super.onDestroy()
    }

}