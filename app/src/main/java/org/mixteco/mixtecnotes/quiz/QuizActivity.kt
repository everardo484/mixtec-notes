package org.mixteco.mixtecnotes.quiz

import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import hugo.weaving.DebugLog
import kotlinx.android.synthetic.main.activity_quiz.*
import org.mixteco.mixtecnotes.R
import org.mixteco.mixtecnotes.common.BaseActivity
import org.mixteco.mixtecnotes.di.ApplicationComponent
import org.mixteco.mixtecnotes.di.subcomponent.quizactivity.QuizActivityModule
import org.mixteco.mixtecnotes.model.Note
import org.mixteco.mixtecnotes.utils.Config
import javax.inject.Inject

class QuizActivity : BaseActivity<QuizActivityView, QuizActivityPresenter>(), QuizActivityView {

    private var mItems: List<Note> = ArrayList()
    private var resetQuiz = true

    @Inject
    lateinit var mPresenter: QuizActivityPresenter

    private lateinit var category: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz)
        category = getCategory()
        setScreenOrientationPortraitIfNeeded()
    }

    override fun onStart() {
        super.onStart()
        if (resetQuiz) {
            reloadNotesIfNeededAndResetQuiz()
            resetQuiz = false
        }
    }

    override fun resetQuiz() {
        val quizFragment = supportFragmentManager.findFragmentById(R.id.quizFragment) as QuizFragment
        quizFragment.updateGuessRows()
        quizFragment.resetQuiz(mItems)
    }

    override fun getPresenter(): QuizActivityPresenter {
        return mPresenter
    }

    override fun onNotesLoading(notes: List<Note>) {
        mItems = notes
    }

    fun reloadNotesIfNeededAndResetQuiz() {
        if (mItems.isEmpty()) {
            presenter.fetch(category)
        } else {
            resetQuiz()
        }
    }

    @DebugLog
    fun getCategory(): String {
        return intent.extras.getString(Config.Fragment.CATEGORY)
    }

    private fun setScreenOrientationPortraitIfNeeded() {
        if (isPhoneDevice()) {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }
    }

    private fun isPhoneDevice(): Boolean {
        val screenSize = resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK
        return (screenSize != Configuration.SCREENLAYOUT_SIZE_LARGE || screenSize != Configuration.SCREENLAYOUT_SIZE_XLARGE)

    }

    override fun showLoadingProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoadingProgressBar() {
        progressBar.visibility = View.GONE
    }

    override fun injectDependencies(applicationComponent: ApplicationComponent?) {
        applicationComponent?.plus(QuizActivityModule(this))?.injectTo(this)
    }
}