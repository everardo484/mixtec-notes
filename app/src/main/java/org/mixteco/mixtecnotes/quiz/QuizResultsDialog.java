package org.mixteco.mixtecnotes.quiz;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

import org.mixteco.mixtecnotes.R;
import org.mixteco.mixtecnotes.utils.Config;

public class QuizResultsDialog extends DialogFragment {
    int totalGuesses;

    @Override
    public void setArguments(@Nullable Bundle args) {
        totalGuesses = args.getInt(Config.Quiz.TOTAL_GUESSES);
        super.setArguments(args);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle bundle) {
        return new AlertDialog.Builder(getActivity())
                .setMessage(getString(R.string.results, totalGuesses, 500 / (double) totalGuesses))
                .setPositiveButton(R.string.reset_quiz, (dialogInterface, i) -> ((QuizActivity) getActivity())
                        .reloadNotesIfNeededAndResetQuiz()).create();
    }
}