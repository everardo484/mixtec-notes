package org.mixteco.mixtecnotes.quiz;

import org.mixteco.mixtecnotes.model.Note;
import org.mixteco.mixtecnotes.mvp.MvpView;

import java.util.List;

public interface QuizActivityView extends MvpView {
    void  onNotesLoading(List<Note> notes);
    void showLoadingProgressBar();
    void hideLoadingProgressBar();
    void resetQuiz();
}