package org.mixteco.mixtecnotes.quiz

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewAnimationUtils
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import org.mixteco.mixtecnotes.R
import org.mixteco.mixtecnotes.common.BaseFragment
import org.mixteco.mixtecnotes.di.ApplicationComponent
import org.mixteco.mixtecnotes.di.subcomponent.quizfragment.QuizFragmentModule
import org.mixteco.mixtecnotes.model.Note
import org.mixteco.mixtecnotes.utils.Config
import java.io.IOException
import java.security.SecureRandom
import javax.inject.Inject

class QuizFragment : BaseFragment<QuizFragmentView, QuizFragmentPresenter>(), QuizFragmentView,
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnCompletionListener,
        View.OnClickListener {

    private val DIALOG_QUIZ_RESULTS = "quiz_results"
    private val WORDS_IN_QUIZ = 5
    private var correctAnswer: Note? = null
    private var totalGuesses: Int = 0
    private var correctAnswers: Int = 0
    private var guessRows: Int = 2
    private var random: SecureRandom? = null
    private var handler: Handler? = null
    private var shakeAnimation: Animation? = null
    private var bounceAnimation: Animation? = null
    private var quizLinearLayout: LinearLayout? = null
    private var questionNumberTextView: TextView? = null
    private var mixtecWordTextView: TextView? = null
    private var regionTextView: TextView? = null
    private var cardQuiz: CardView? = null
    private var listenImageView: ImageView? = null
    private var playImageView: ImageView? = null
    private var guessLinearLayouts: Array<LinearLayout?>? = null
    private var answerTextView: TextView? = null
    private var quizWordList: MutableList<Note> = ArrayList()

    @Inject
    lateinit var mPresenter: QuizFragmentPresenter

    @Inject
    lateinit var mPlayer: MediaPlayer


    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.fragment_quiz, container, false)

        random = SecureRandom()
        handler = Handler()

        shakeAnimation = AnimationUtils.loadAnimation(activity, R.anim.incorrect_shake)
        shakeAnimation?.setRepeatCount(3)

        bounceAnimation = AnimationUtils.loadAnimation(activity, R.anim.bounce)

        mPlayer.setOnPreparedListener(this)
        mPlayer.setOnCompletionListener(this)

        quizLinearLayout = view.findViewById<View>(R.id.quizLinearLayout) as LinearLayout
        questionNumberTextView = view.findViewById<View>(R.id.questionNumberTextView) as TextView
        mixtecWordTextView = view.findViewById<View>(R.id.theWord) as TextView
        listenImageView = view.findViewById<View>(R.id.playAudio) as ImageView
        playImageView = view.findViewById(R.id.playAudio) as ImageView
        cardQuiz = view.findViewById(R.id.cardQuiz) as CardView
        regionTextView = view.findViewById(R.id.regionTextView) as TextView

        guessLinearLayouts = arrayOfNulls(guessRows)
        guessLinearLayouts!![0] = view.findViewById<View>(R.id.row1LinearLayout) as LinearLayout
        guessLinearLayouts!![1] = view.findViewById<View>(R.id.row2LinearLayout) as LinearLayout
        answerTextView = view.findViewById<View>(R.id.answerTextView) as TextView

        for (row in guessLinearLayouts!!) {
            for (column in 0 until row!!.getChildCount()) {
                val button = row.getChildAt(column) as Button
                button.setOnClickListener(guessButtonListener)
            }
        }
        questionNumberTextView?.setText(getString(R.string.question, 1, WORDS_IN_QUIZ))
        playImageView?.setOnClickListener(this)

        return view
    }

    fun updateGuessRows() {
        for (layout in this.guessLinearLayouts!!)
            layout?.setVisibility(View.GONE)

        for (row in 0 until guessRows) {
            guessLinearLayouts?.get(row)?.setVisibility(View.VISIBLE)
        }
    }

    fun resetQuiz(notes: List<Note>) {
        correctAnswers = 0
        totalGuesses = 0
        quizWordList.clear()
        quizWordList.addAll(notes)
        loadNextWord()
    }

    private val guessButtonListener = View.OnClickListener { v ->
        val guessButton = v as Button
        val guess = guessButton.text.toString()
        val answer = correctAnswer?.englishWord.toString()
        ++totalGuesses

        if (guess == answer) {
            ++correctAnswers
            answerTextView?.setText(getString(R.string.correct_answer))
            answerTextView?.setTextColor(resources.getColor(R.color.correct_answer, context!!.theme))
            disableButtons()

            if (correctAnswers == WORDS_IN_QUIZ) {
                val fm = activity!!.supportFragmentManager
                var bundle = Bundle();
                bundle.putInt(Config.Quiz.TOTAL_GUESSES, totalGuesses)
                val quizResults = QuizResultsDialog()
                quizResults.arguments = bundle
                quizResults.isCancelable = false
                quizResults.show(fm, DIALOG_QUIZ_RESULTS);
            } else {
                handler?.postDelayed({
                    animate(true)
                }, 2000)
            }
        } else {
            cardQuiz?.startAnimation(shakeAnimation)
            answerTextView?.setText(R.string.incorrect_answer)
            answerTextView?.setTextColor(resources.getColor(R.color.incorrect_answer, context!!.theme))
            guessButton.isEnabled = false
        }
    }

    private fun animate(animateOut: Boolean) {
        if (correctAnswers == 0) {
            return
        }

        val centerX = (quizLinearLayout!!.getLeft() + quizLinearLayout!!.getRight()) / 2
        val centerY = (quizLinearLayout!!.getTop() + quizLinearLayout!!.getBottom()) / 2
        val radius = Math.max(quizLinearLayout!!.getWidth(), quizLinearLayout!!.getHeight())

        val animator: Animator

        if (animateOut) {
            animator = ViewAnimationUtils.createCircularReveal(quizLinearLayout, centerX, centerY, radius.toFloat(), 0f)
            animator.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    loadNextWord()
                }
            })
        } else {
            animator = ViewAnimationUtils.createCircularReveal(quizLinearLayout, centerX, centerY, 0f, radius.toFloat())
        }

        animator.duration = 500
        animator.start()
    }

    private fun disableButtons() {
        for (row in 0 until guessRows) {
            val guessRow = guessLinearLayouts?.get(row)
            for (i in 0 until guessRow!!.getChildCount())
                guessRow.getChildAt(i).isEnabled = false
        }
    }

    private fun loadNextWord() {
        quizWordList.shuffle()
        val nextWord = quizWordList.removeAt(0)
        correctAnswer = nextWord
        answerTextView?.text = ""
        regionTextView?.text = nextWord.region

        if (!nextWord.hasAudio()) {
            playImageView?.visibility = View.GONE
        } else {
            playImageView?.visibility = View.VISIBLE
        }

        questionNumberTextView?.setText(getString(R.string.question, correctAnswers + 1, WORDS_IN_QUIZ))

        mixtecWordTextView?.setText(nextWord.mixtecTranslation)
        animate(false)

        for (row in 0 until guessRows) {
            for (column in 0 until guessLinearLayouts!![row]?.getChildCount()!!) {
                val newGuessButton = guessLinearLayouts!![row]?.getChildAt(column) as Button
                newGuessButton.isEnabled = true
                val note = quizWordList.get(row * 2 + column)
                newGuessButton.setText(note.englishWord)
            }
        }

        val row = random?.nextInt(guessRows)
        val column = random?.nextInt(2)
        val randomRow = guessLinearLayouts!![row!!]

        (randomRow?.getChildAt(column!!) as Button).setText(correctAnswer?.englishWord)
    }

    override fun onClick(view: View?) {
        if (view?.id == R.id.playAudio) {
            view.startAnimation(bounceAnimation)
            playAudio(correctAnswer)
        }
    }

    fun playAudio(note: Note?) {
        mPlayer.reset()
        try {
            mPlayer.setDataSource(note?.audio)
            mPlayer.prepareAsync()
        } catch (e: IOException) {
        }
    }

    override fun onPrepared(player: MediaPlayer?) {
        mPlayer.start()
    }

    override fun onCompletion(mp: MediaPlayer?) {
        mPlayer.stop()
        mPlayer.reset()
    }

    override fun getPresenter(): QuizFragmentPresenter {
        return mPresenter
    }

    override fun onDestroyView() {
        handler?.removeCallbacksAndMessages(null)
        super.onDestroyView()
    }

    override fun injectDependencies(applicationComponent: ApplicationComponent?) {
        applicationComponent?.plus(QuizFragmentModule(this))?.injectTo(this)
    }
}